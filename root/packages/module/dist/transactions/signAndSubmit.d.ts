import { BrowserWallet } from '@meshsdk/core';
/**
 *
 * @param {BrowserWallet} wallet
 * @param {string} unsignedTx
 * @returns txHash of successful transaction
 * @error if transaction fails
 */
export declare function SignAndSubmit(wallet: BrowserWallet, unsignedTx: string): Promise<string>;
//# sourceMappingURL=signAndSubmit.d.ts.map