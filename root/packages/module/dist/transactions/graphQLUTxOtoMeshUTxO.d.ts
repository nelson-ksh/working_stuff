import { UTxO } from '@meshsdk/core';
import { GraphQLUTxO } from 'src/types';
/**
 * Translate a GraphQL query response into a Mesh UTxO
 * @param {GraphQLUTxO} utxoFromQuery a UTxO as returned by GraphQL query
 * @returns {UTxO} a Mesh UTxO
 */
export declare function graphQLUTxOtoMeshUTxO({ utxoFromQuery, }: {
    utxoFromQuery: GraphQLUTxO;
}): UTxO;
//# sourceMappingURL=graphQLUTxOtoMeshUTxO.d.ts.map