import { AssetExtended, BrowserWallet } from '@meshsdk/core';
/**
 *
 * @param {BrowserWallet} wallet
 * @param {string} policyId
 * @returns the contributor AssetExtended in the connected wallet
 */
export declare function getConnectedToken({ wallet, policyId, }: {
    wallet: BrowserWallet;
    policyId: string;
}): Promise<AssetExtended>;
//# sourceMappingURL=getConnectedToken.d.ts.map