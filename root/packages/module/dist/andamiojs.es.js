var We = Object.defineProperty;
var Ke = (e, t, n) => t in e ? We(e, t, { enumerable: !0, configurable: !0, writable: !0, value: n }) : e[t] = n;
var x = (e, t, n) => (Ke(e, typeof t != "symbol" ? t + "" : t, n), n);
import { resolvePaymentKeyHash as ge, resolveStakeKeyHash as Ze, resolveScriptRef as J, Transaction as me, resolvePlutusScriptHash as et } from "@meshsdk/core";
function tt(e, t) {
  for (var n = 0; n < t.length; n++) {
    const s = t[n];
    if (typeof s != "string" && !Array.isArray(s)) {
      for (const r in s)
        if (r !== "default" && !(r in e)) {
          const i = Object.getOwnPropertyDescriptor(s, r);
          i && Object.defineProperty(e, r, i.get ? i : {
            enumerable: !0,
            get: () => s[r]
          });
        }
    }
  }
  return Object.freeze(Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }));
}
async function Re(e, t) {
  try {
    const n = await e.signTx(await t, !0), s = await e.submitTx(n);
    return console.log("Success!", s), s;
  } catch (n) {
    if (n instanceof Error) {
      const s = n;
      s.info ? alert(s.info) : console.log(n);
    } else
      console.error(n);
    return "Error Signing/Submitting";
  }
}
async function Gn({
  wallet: e,
  policyId: t
}) {
  let n;
  const s = await e.getPolicyIdAssets(t);
  if (s.length > 0 && (n = s[0]), n === void 0)
    throw new Error("Contributor Token not found");
  return n;
}
function zn(e) {
  const t = [];
  for (let n = 0; n < e.length; n++)
    t[n] = e.charCodeAt(n).toString(16);
  return t.join("");
}
function nt(e) {
  const t = e.toString();
  let n = "";
  for (let s = 0; s < t.length; s += 2)
    n += String.fromCharCode(parseInt(t.substr(s, 2), 16));
  return n;
}
function Yn(e) {
  const t = new Date(e);
  return `${t.getFullYear()}-${(t.getMonth() + 1).toString().padStart(2, "0")}-${(t.getDate() + 1).toString().padStart(2, "0")}`;
}
function Xn(e) {
  const t = [];
  return e.tokens.forEach((n) => {
    const s = n.asset.policyId + n.asset.assetName, r = n.quantity;
    n.asset.fingerprint && t.push({
      unit: s,
      policyId: n.asset.policyId,
      assetName: nt(n.asset.assetName),
      fingerprint: n.asset.fingerprint,
      quantity: r
    });
  }), t;
}
function Jn({ months: e }) {
  const t = new Date(Date.now()), n = t.getMonth();
  return t.setMonth(n + e), t.valueOf();
}
function Wn(e, t) {
  const n = t.find(
    (s) => s.contractTokenName === e
  );
  if (n)
    return n;
}
const ye = JSON, st = (e) => e.toUpperCase(), rt = (e) => {
  const t = {};
  return e.forEach((n, s) => {
    t[s] = n;
  }), t;
}, it = (e, t, n) => e.document ? e : {
  document: e,
  variables: t,
  requestHeaders: n,
  signal: void 0
}, ot = (e, t, n) => e.query ? e : {
  query: e,
  variables: t,
  requestHeaders: n,
  signal: void 0
}, at = (e, t) => e.documents ? e : {
  documents: e,
  requestHeaders: t,
  signal: void 0
};
function te(e, t) {
  if (!Boolean(e))
    throw new Error(t);
}
function ct(e) {
  return typeof e == "object" && e !== null;
}
function ut(e, t) {
  if (!Boolean(e))
    throw new Error(
      t != null ? t : "Unexpected invariant triggered."
    );
}
const dt = /\r\n|[\n\r]/g;
function fe(e, t) {
  let n = 0, s = 1;
  for (const r of e.body.matchAll(dt)) {
    if (typeof r.index == "number" || ut(!1), r.index >= t)
      break;
    n = r.index + r[0].length, s += 1;
  }
  return {
    line: s,
    column: t + 1 - n
  };
}
function lt(e) {
  return Se(
    e.source,
    fe(e.source, e.start)
  );
}
function Se(e, t) {
  const n = e.locationOffset.column - 1, s = "".padStart(n) + e.body, r = t.line - 1, i = e.locationOffset.line - 1, o = t.line + i, c = t.line === 1 ? n : 0, d = t.column + c, l = `${e.name}:${o}:${d}
`, h = s.split(/\r\n|[\n\r]/g), T = h[r];
  if (T.length > 120) {
    const E = Math.floor(d / 80), b = d % 80, m = [];
    for (let A = 0; A < T.length; A += 80)
      m.push(T.slice(A, A + 80));
    return l + Ae([
      [`${o} |`, m[0]],
      ...m.slice(1, E + 1).map((A) => ["|", A]),
      ["|", "^".padStart(b)],
      ["|", m[E + 1]]
    ]);
  }
  return l + Ae([
    [`${o - 1} |`, h[r - 1]],
    [`${o} |`, T],
    ["|", "^".padStart(d)],
    [`${o + 1} |`, h[r + 1]]
  ]);
}
function Ae(e) {
  const t = e.filter(([s, r]) => r !== void 0), n = Math.max(...t.map(([s]) => s.length));
  return t.map(([s, r]) => s.padStart(n) + (r ? " " + r : "")).join(`
`);
}
function ht(e) {
  const t = e[0];
  return t == null || "kind" in t || "length" in t ? {
    nodes: t,
    source: e[1],
    positions: e[2],
    path: e[3],
    originalError: e[4],
    extensions: e[5]
  } : t;
}
class Ee extends Error {
  constructor(t, ...n) {
    var s, r, i;
    const { nodes: o, source: c, positions: d, path: l, originalError: h, extensions: T } = ht(n);
    super(t), this.name = "GraphQLError", this.path = l != null ? l : void 0, this.originalError = h != null ? h : void 0, this.nodes = _e(
      Array.isArray(o) ? o : o ? [o] : void 0
    );
    const E = _e(
      (s = this.nodes) === null || s === void 0 ? void 0 : s.map((m) => m.loc).filter((m) => m != null)
    );
    this.source = c != null ? c : E == null || (r = E[0]) === null || r === void 0 ? void 0 : r.source, this.positions = d != null ? d : E == null ? void 0 : E.map((m) => m.start), this.locations = d && c ? d.map((m) => fe(c, m)) : E == null ? void 0 : E.map((m) => fe(m.source, m.start));
    const b = ct(
      h == null ? void 0 : h.extensions
    ) ? h == null ? void 0 : h.extensions : void 0;
    this.extensions = (i = T != null ? T : b) !== null && i !== void 0 ? i : /* @__PURE__ */ Object.create(null), Object.defineProperties(this, {
      message: {
        writable: !0,
        enumerable: !0
      },
      name: {
        enumerable: !1
      },
      nodes: {
        enumerable: !1
      },
      source: {
        enumerable: !1
      },
      positions: {
        enumerable: !1
      },
      originalError: {
        enumerable: !1
      }
    }), h != null && h.stack ? Object.defineProperty(this, "stack", {
      value: h.stack,
      writable: !0,
      configurable: !0
    }) : Error.captureStackTrace ? Error.captureStackTrace(this, Ee) : Object.defineProperty(this, "stack", {
      value: Error().stack,
      writable: !0,
      configurable: !0
    });
  }
  get [Symbol.toStringTag]() {
    return "GraphQLError";
  }
  toString() {
    let t = this.message;
    if (this.nodes)
      for (const n of this.nodes)
        n.loc && (t += `

` + lt(n.loc));
    else if (this.source && this.locations)
      for (const n of this.locations)
        t += `

` + Se(this.source, n);
    return t;
  }
  toJSON() {
    const t = {
      message: this.message
    };
    return this.locations != null && (t.locations = this.locations), this.path != null && (t.path = this.path), this.extensions != null && Object.keys(this.extensions).length > 0 && (t.extensions = this.extensions), t;
  }
}
function _e(e) {
  return e === void 0 || e.length === 0 ? void 0 : e;
}
function C(e, t, n) {
  return new Ee(`Syntax Error: ${n}`, {
    source: e,
    positions: [t]
  });
}
class ft {
  constructor(t, n, s) {
    this.start = t.start, this.end = n.end, this.startToken = t, this.endToken = n, this.source = s;
  }
  get [Symbol.toStringTag]() {
    return "Location";
  }
  toJSON() {
    return {
      start: this.start,
      end: this.end
    };
  }
}
class De {
  constructor(t, n, s, r, i, o) {
    this.kind = t, this.start = n, this.end = s, this.line = r, this.column = i, this.value = o, this.prev = null, this.next = null;
  }
  get [Symbol.toStringTag]() {
    return "Token";
  }
  toJSON() {
    return {
      kind: this.kind,
      value: this.value,
      line: this.line,
      column: this.column
    };
  }
}
const Ue = {
  Name: [],
  Document: ["definitions"],
  OperationDefinition: [
    "name",
    "variableDefinitions",
    "directives",
    "selectionSet"
  ],
  VariableDefinition: ["variable", "type", "defaultValue", "directives"],
  Variable: ["name"],
  SelectionSet: ["selections"],
  Field: ["alias", "name", "arguments", "directives", "selectionSet"],
  Argument: ["name", "value"],
  FragmentSpread: ["name", "directives"],
  InlineFragment: ["typeCondition", "directives", "selectionSet"],
  FragmentDefinition: [
    "name",
    "variableDefinitions",
    "typeCondition",
    "directives",
    "selectionSet"
  ],
  IntValue: [],
  FloatValue: [],
  StringValue: [],
  BooleanValue: [],
  NullValue: [],
  EnumValue: [],
  ListValue: ["values"],
  ObjectValue: ["fields"],
  ObjectField: ["name", "value"],
  Directive: ["name", "arguments"],
  NamedType: ["name"],
  ListType: ["type"],
  NonNullType: ["type"],
  SchemaDefinition: ["description", "directives", "operationTypes"],
  OperationTypeDefinition: ["type"],
  ScalarTypeDefinition: ["description", "name", "directives"],
  ObjectTypeDefinition: [
    "description",
    "name",
    "interfaces",
    "directives",
    "fields"
  ],
  FieldDefinition: ["description", "name", "arguments", "type", "directives"],
  InputValueDefinition: [
    "description",
    "name",
    "type",
    "defaultValue",
    "directives"
  ],
  InterfaceTypeDefinition: [
    "description",
    "name",
    "interfaces",
    "directives",
    "fields"
  ],
  UnionTypeDefinition: ["description", "name", "directives", "types"],
  EnumTypeDefinition: ["description", "name", "directives", "values"],
  EnumValueDefinition: ["description", "name", "directives"],
  InputObjectTypeDefinition: ["description", "name", "directives", "fields"],
  DirectiveDefinition: ["description", "name", "arguments", "locations"],
  SchemaExtension: ["directives", "operationTypes"],
  ScalarTypeExtension: ["name", "directives"],
  ObjectTypeExtension: ["name", "interfaces", "directives", "fields"],
  InterfaceTypeExtension: ["name", "interfaces", "directives", "fields"],
  UnionTypeExtension: ["name", "directives", "types"],
  EnumTypeExtension: ["name", "directives", "values"],
  InputObjectTypeExtension: ["name", "directives", "fields"]
}, pt = new Set(Object.keys(Ue));
function Ce(e) {
  const t = e == null ? void 0 : e.kind;
  return typeof t == "string" && pt.has(t);
}
var H;
(function(e) {
  e.QUERY = "query", e.MUTATION = "mutation", e.SUBSCRIPTION = "subscription";
})(H || (H = {}));
var pe;
(function(e) {
  e.QUERY = "QUERY", e.MUTATION = "MUTATION", e.SUBSCRIPTION = "SUBSCRIPTION", e.FIELD = "FIELD", e.FRAGMENT_DEFINITION = "FRAGMENT_DEFINITION", e.FRAGMENT_SPREAD = "FRAGMENT_SPREAD", e.INLINE_FRAGMENT = "INLINE_FRAGMENT", e.VARIABLE_DEFINITION = "VARIABLE_DEFINITION", e.SCHEMA = "SCHEMA", e.SCALAR = "SCALAR", e.OBJECT = "OBJECT", e.FIELD_DEFINITION = "FIELD_DEFINITION", e.ARGUMENT_DEFINITION = "ARGUMENT_DEFINITION", e.INTERFACE = "INTERFACE", e.UNION = "UNION", e.ENUM = "ENUM", e.ENUM_VALUE = "ENUM_VALUE", e.INPUT_OBJECT = "INPUT_OBJECT", e.INPUT_FIELD_DEFINITION = "INPUT_FIELD_DEFINITION";
})(pe || (pe = {}));
var y;
(function(e) {
  e.NAME = "Name", e.DOCUMENT = "Document", e.OPERATION_DEFINITION = "OperationDefinition", e.VARIABLE_DEFINITION = "VariableDefinition", e.SELECTION_SET = "SelectionSet", e.FIELD = "Field", e.ARGUMENT = "Argument", e.FRAGMENT_SPREAD = "FragmentSpread", e.INLINE_FRAGMENT = "InlineFragment", e.FRAGMENT_DEFINITION = "FragmentDefinition", e.VARIABLE = "Variable", e.INT = "IntValue", e.FLOAT = "FloatValue", e.STRING = "StringValue", e.BOOLEAN = "BooleanValue", e.NULL = "NullValue", e.ENUM = "EnumValue", e.LIST = "ListValue", e.OBJECT = "ObjectValue", e.OBJECT_FIELD = "ObjectField", e.DIRECTIVE = "Directive", e.NAMED_TYPE = "NamedType", e.LIST_TYPE = "ListType", e.NON_NULL_TYPE = "NonNullType", e.SCHEMA_DEFINITION = "SchemaDefinition", e.OPERATION_TYPE_DEFINITION = "OperationTypeDefinition", e.SCALAR_TYPE_DEFINITION = "ScalarTypeDefinition", e.OBJECT_TYPE_DEFINITION = "ObjectTypeDefinition", e.FIELD_DEFINITION = "FieldDefinition", e.INPUT_VALUE_DEFINITION = "InputValueDefinition", e.INTERFACE_TYPE_DEFINITION = "InterfaceTypeDefinition", e.UNION_TYPE_DEFINITION = "UnionTypeDefinition", e.ENUM_TYPE_DEFINITION = "EnumTypeDefinition", e.ENUM_VALUE_DEFINITION = "EnumValueDefinition", e.INPUT_OBJECT_TYPE_DEFINITION = "InputObjectTypeDefinition", e.DIRECTIVE_DEFINITION = "DirectiveDefinition", e.SCHEMA_EXTENSION = "SchemaExtension", e.SCALAR_TYPE_EXTENSION = "ScalarTypeExtension", e.OBJECT_TYPE_EXTENSION = "ObjectTypeExtension", e.INTERFACE_TYPE_EXTENSION = "InterfaceTypeExtension", e.UNION_TYPE_EXTENSION = "UnionTypeExtension", e.ENUM_TYPE_EXTENSION = "EnumTypeExtension", e.INPUT_OBJECT_TYPE_EXTENSION = "InputObjectTypeExtension";
})(y || (y = {}));
function Te(e) {
  return e === 9 || e === 32;
}
function X(e) {
  return e >= 48 && e <= 57;
}
function Le(e) {
  return e >= 97 && e <= 122 || e >= 65 && e <= 90;
}
function Fe(e) {
  return Le(e) || e === 95;
}
function Tt(e) {
  return Le(e) || X(e) || e === 95;
}
function mt(e) {
  var t;
  let n = Number.MAX_SAFE_INTEGER, s = null, r = -1;
  for (let o = 0; o < e.length; ++o) {
    var i;
    const c = e[o], d = yt(c);
    d !== c.length && (s = (i = s) !== null && i !== void 0 ? i : o, r = o, o !== 0 && d < n && (n = d));
  }
  return e.map((o, c) => c === 0 ? o : o.slice(n)).slice(
    (t = s) !== null && t !== void 0 ? t : 0,
    r + 1
  );
}
function yt(e) {
  let t = 0;
  for (; t < e.length && Te(e.charCodeAt(t)); )
    ++t;
  return t;
}
function Et(e, t) {
  const n = e.replace(/"""/g, '\\"""'), s = n.split(/\r\n|[\n\r]/g), r = s.length === 1, i = s.length > 1 && s.slice(1).every((b) => b.length === 0 || Te(b.charCodeAt(0))), o = n.endsWith('\\"""'), c = e.endsWith('"') && !o, d = e.endsWith("\\"), l = c || d, h = !(t != null && t.minimize) && (!r || e.length > 70 || l || i || o);
  let T = "";
  const E = r && Te(e.charCodeAt(0));
  return (h && !E || i) && (T += `
`), T += n, (h || l) && (T += `
`), '"""' + T + '"""';
}
var u;
(function(e) {
  e.SOF = "<SOF>", e.EOF = "<EOF>", e.BANG = "!", e.DOLLAR = "$", e.AMP = "&", e.PAREN_L = "(", e.PAREN_R = ")", e.SPREAD = "...", e.COLON = ":", e.EQUALS = "=", e.AT = "@", e.BRACKET_L = "[", e.BRACKET_R = "]", e.BRACE_L = "{", e.PIPE = "|", e.BRACE_R = "}", e.NAME = "Name", e.INT = "Int", e.FLOAT = "Float", e.STRING = "String", e.BLOCK_STRING = "BlockString", e.COMMENT = "Comment";
})(u || (u = {}));
class xt {
  constructor(t) {
    const n = new De(u.SOF, 0, 0, 0, 0);
    this.source = t, this.lastToken = n, this.token = n, this.line = 1, this.lineStart = 0;
  }
  get [Symbol.toStringTag]() {
    return "Lexer";
  }
  advance() {
    return this.lastToken = this.token, this.token = this.lookahead();
  }
  lookahead() {
    let t = this.token;
    if (t.kind !== u.EOF)
      do
        if (t.next)
          t = t.next;
        else {
          const n = Ot(this, t.end);
          t.next = n, n.prev = t, t = n;
        }
      while (t.kind === u.COMMENT);
    return t;
  }
}
function vt(e) {
  return e === u.BANG || e === u.DOLLAR || e === u.AMP || e === u.PAREN_L || e === u.PAREN_R || e === u.SPREAD || e === u.COLON || e === u.EQUALS || e === u.AT || e === u.BRACKET_L || e === u.BRACKET_R || e === u.BRACE_L || e === u.PIPE || e === u.BRACE_R;
}
function $(e) {
  return e >= 0 && e <= 55295 || e >= 57344 && e <= 1114111;
}
function re(e, t) {
  return Pe(e.charCodeAt(t)) && Be(e.charCodeAt(t + 1));
}
function Pe(e) {
  return e >= 55296 && e <= 56319;
}
function Be(e) {
  return e >= 56320 && e <= 57343;
}
function M(e, t) {
  const n = e.source.body.codePointAt(t);
  if (n === void 0)
    return u.EOF;
  if (n >= 32 && n <= 126) {
    const s = String.fromCodePoint(n);
    return s === '"' ? `'"'` : `"${s}"`;
  }
  return "U+" + n.toString(16).toUpperCase().padStart(4, "0");
}
function _(e, t, n, s, r) {
  const i = e.line, o = 1 + n - e.lineStart;
  return new De(t, n, s, i, o, r);
}
function Ot(e, t) {
  const n = e.source.body, s = n.length;
  let r = t;
  for (; r < s; ) {
    const i = n.charCodeAt(r);
    switch (i) {
      case 65279:
      case 9:
      case 32:
      case 44:
        ++r;
        continue;
      case 10:
        ++r, ++e.line, e.lineStart = r;
        continue;
      case 13:
        n.charCodeAt(r + 1) === 10 ? r += 2 : ++r, ++e.line, e.lineStart = r;
        continue;
      case 35:
        return bt(e, r);
      case 33:
        return _(e, u.BANG, r, r + 1);
      case 36:
        return _(e, u.DOLLAR, r, r + 1);
      case 38:
        return _(e, u.AMP, r, r + 1);
      case 40:
        return _(e, u.PAREN_L, r, r + 1);
      case 41:
        return _(e, u.PAREN_R, r, r + 1);
      case 46:
        if (n.charCodeAt(r + 1) === 46 && n.charCodeAt(r + 2) === 46)
          return _(e, u.SPREAD, r, r + 3);
        break;
      case 58:
        return _(e, u.COLON, r, r + 1);
      case 61:
        return _(e, u.EQUALS, r, r + 1);
      case 64:
        return _(e, u.AT, r, r + 1);
      case 91:
        return _(e, u.BRACKET_L, r, r + 1);
      case 93:
        return _(e, u.BRACKET_R, r, r + 1);
      case 123:
        return _(e, u.BRACE_L, r, r + 1);
      case 124:
        return _(e, u.PIPE, r, r + 1);
      case 125:
        return _(e, u.BRACE_R, r, r + 1);
      case 34:
        return n.charCodeAt(r + 1) === 34 && n.charCodeAt(r + 2) === 34 ? Nt(e, r) : At(e, r);
    }
    if (X(i) || i === 45)
      return gt(e, r, i);
    if (Fe(i))
      return wt(e, r);
    throw C(
      e.source,
      r,
      i === 39 ? `Unexpected single quote character ('), did you mean to use a double quote (")?` : $(i) || re(n, r) ? `Unexpected character: ${M(e, r)}.` : `Invalid character: ${M(e, r)}.`
    );
  }
  return _(e, u.EOF, s, s);
}
function bt(e, t) {
  const n = e.source.body, s = n.length;
  let r = t + 1;
  for (; r < s; ) {
    const i = n.charCodeAt(r);
    if (i === 10 || i === 13)
      break;
    if ($(i))
      ++r;
    else if (re(n, r))
      r += 2;
    else
      break;
  }
  return _(
    e,
    u.COMMENT,
    t,
    r,
    n.slice(t + 1, r)
  );
}
function gt(e, t, n) {
  const s = e.source.body;
  let r = t, i = n, o = !1;
  if (i === 45 && (i = s.charCodeAt(++r)), i === 48) {
    if (i = s.charCodeAt(++r), X(i))
      throw C(
        e.source,
        r,
        `Invalid number, unexpected digit after 0: ${M(
          e,
          r
        )}.`
      );
  } else
    r = ue(e, r, i), i = s.charCodeAt(r);
  if (i === 46 && (o = !0, i = s.charCodeAt(++r), r = ue(e, r, i), i = s.charCodeAt(r)), (i === 69 || i === 101) && (o = !0, i = s.charCodeAt(++r), (i === 43 || i === 45) && (i = s.charCodeAt(++r)), r = ue(e, r, i), i = s.charCodeAt(r)), i === 46 || Fe(i))
    throw C(
      e.source,
      r,
      `Invalid number, expected digit but got: ${M(
        e,
        r
      )}.`
    );
  return _(
    e,
    o ? u.FLOAT : u.INT,
    t,
    r,
    s.slice(t, r)
  );
}
function ue(e, t, n) {
  if (!X(n))
    throw C(
      e.source,
      t,
      `Invalid number, expected digit but got: ${M(
        e,
        t
      )}.`
    );
  const s = e.source.body;
  let r = t + 1;
  for (; X(s.charCodeAt(r)); )
    ++r;
  return r;
}
function At(e, t) {
  const n = e.source.body, s = n.length;
  let r = t + 1, i = r, o = "";
  for (; r < s; ) {
    const c = n.charCodeAt(r);
    if (c === 34)
      return o += n.slice(i, r), _(e, u.STRING, t, r + 1, o);
    if (c === 92) {
      o += n.slice(i, r);
      const d = n.charCodeAt(r + 1) === 117 ? n.charCodeAt(r + 2) === 123 ? _t(e, r) : Ct(e, r) : It(e, r);
      o += d.value, r += d.size, i = r;
      continue;
    }
    if (c === 10 || c === 13)
      break;
    if ($(c))
      ++r;
    else if (re(n, r))
      r += 2;
    else
      throw C(
        e.source,
        r,
        `Invalid character within String: ${M(
          e,
          r
        )}.`
      );
  }
  throw C(e.source, r, "Unterminated string.");
}
function _t(e, t) {
  const n = e.source.body;
  let s = 0, r = 3;
  for (; r < 12; ) {
    const i = n.charCodeAt(t + r++);
    if (i === 125) {
      if (r < 5 || !$(s))
        break;
      return {
        value: String.fromCodePoint(s),
        size: r
      };
    }
    if (s = s << 4 | z(i), s < 0)
      break;
  }
  throw C(
    e.source,
    t,
    `Invalid Unicode escape sequence: "${n.slice(
      t,
      t + r
    )}".`
  );
}
function Ct(e, t) {
  const n = e.source.body, s = Ie(n, t + 2);
  if ($(s))
    return {
      value: String.fromCodePoint(s),
      size: 6
    };
  if (Pe(s) && n.charCodeAt(t + 6) === 92 && n.charCodeAt(t + 7) === 117) {
    const r = Ie(n, t + 8);
    if (Be(r))
      return {
        value: String.fromCodePoint(s, r),
        size: 12
      };
  }
  throw C(
    e.source,
    t,
    `Invalid Unicode escape sequence: "${n.slice(t, t + 6)}".`
  );
}
function Ie(e, t) {
  return z(e.charCodeAt(t)) << 12 | z(e.charCodeAt(t + 1)) << 8 | z(e.charCodeAt(t + 2)) << 4 | z(e.charCodeAt(t + 3));
}
function z(e) {
  return e >= 48 && e <= 57 ? e - 48 : e >= 65 && e <= 70 ? e - 55 : e >= 97 && e <= 102 ? e - 87 : -1;
}
function It(e, t) {
  const n = e.source.body;
  switch (n.charCodeAt(t + 1)) {
    case 34:
      return {
        value: '"',
        size: 2
      };
    case 92:
      return {
        value: "\\",
        size: 2
      };
    case 47:
      return {
        value: "/",
        size: 2
      };
    case 98:
      return {
        value: "\b",
        size: 2
      };
    case 102:
      return {
        value: "\f",
        size: 2
      };
    case 110:
      return {
        value: `
`,
        size: 2
      };
    case 114:
      return {
        value: "\r",
        size: 2
      };
    case 116:
      return {
        value: "	",
        size: 2
      };
  }
  throw C(
    e.source,
    t,
    `Invalid character escape sequence: "${n.slice(
      t,
      t + 2
    )}".`
  );
}
function Nt(e, t) {
  const n = e.source.body, s = n.length;
  let r = e.lineStart, i = t + 3, o = i, c = "";
  const d = [];
  for (; i < s; ) {
    const l = n.charCodeAt(i);
    if (l === 34 && n.charCodeAt(i + 1) === 34 && n.charCodeAt(i + 2) === 34) {
      c += n.slice(o, i), d.push(c);
      const h = _(
        e,
        u.BLOCK_STRING,
        t,
        i + 3,
        mt(d).join(`
`)
      );
      return e.line += d.length - 1, e.lineStart = r, h;
    }
    if (l === 92 && n.charCodeAt(i + 1) === 34 && n.charCodeAt(i + 2) === 34 && n.charCodeAt(i + 3) === 34) {
      c += n.slice(o, i), o = i + 1, i += 4;
      continue;
    }
    if (l === 10 || l === 13) {
      c += n.slice(o, i), d.push(c), l === 13 && n.charCodeAt(i + 1) === 10 ? i += 2 : ++i, c = "", o = i, r = i;
      continue;
    }
    if ($(l))
      ++i;
    else if (re(n, i))
      i += 2;
    else
      throw C(
        e.source,
        i,
        `Invalid character within String: ${M(
          e,
          i
        )}.`
      );
  }
  throw C(e.source, i, "Unterminated string.");
}
function wt(e, t) {
  const n = e.source.body, s = n.length;
  let r = t + 1;
  for (; r < s; ) {
    const i = n.charCodeAt(r);
    if (Tt(i))
      ++r;
    else
      break;
  }
  return _(
    e,
    u.NAME,
    t,
    r,
    n.slice(t, r)
  );
}
const kt = 10, Me = 2;
function xe(e) {
  return ie(e, []);
}
function ie(e, t) {
  switch (typeof e) {
    case "string":
      return JSON.stringify(e);
    case "function":
      return e.name ? `[function ${e.name}]` : "[function]";
    case "object":
      return Rt(e, t);
    default:
      return String(e);
  }
}
function Rt(e, t) {
  if (e === null)
    return "null";
  if (t.includes(e))
    return "[Circular]";
  const n = [...t, e];
  if (St(e)) {
    const s = e.toJSON();
    if (s !== e)
      return typeof s == "string" ? s : ie(s, n);
  } else if (Array.isArray(e))
    return Ut(e, n);
  return Dt(e, n);
}
function St(e) {
  return typeof e.toJSON == "function";
}
function Dt(e, t) {
  const n = Object.entries(e);
  return n.length === 0 ? "{}" : t.length > Me ? "[" + Lt(e) + "]" : "{ " + n.map(
    ([r, i]) => r + ": " + ie(i, t)
  ).join(", ") + " }";
}
function Ut(e, t) {
  if (e.length === 0)
    return "[]";
  if (t.length > Me)
    return "[Array]";
  const n = Math.min(kt, e.length), s = e.length - n, r = [];
  for (let i = 0; i < n; ++i)
    r.push(ie(e[i], t));
  return s === 1 ? r.push("... 1 more item") : s > 1 && r.push(`... ${s} more items`), "[" + r.join(", ") + "]";
}
function Lt(e) {
  const t = Object.prototype.toString.call(e).replace(/^\[object /, "").replace(/]$/, "");
  if (t === "Object" && typeof e.constructor == "function") {
    const n = e.constructor.name;
    if (typeof n == "string" && n !== "")
      return n;
  }
  return t;
}
const Ft = globalThis.process && globalThis.process.env.NODE_ENV === "production" ? function(t, n) {
  return t instanceof n;
} : function(t, n) {
  if (t instanceof n)
    return !0;
  if (typeof t == "object" && t !== null) {
    var s;
    const r = n.prototype[Symbol.toStringTag], i = Symbol.toStringTag in t ? t[Symbol.toStringTag] : (s = t.constructor) === null || s === void 0 ? void 0 : s.name;
    if (r === i) {
      const o = xe(t);
      throw new Error(`Cannot use ${r} "${o}" from another module or realm.

Ensure that there is only one instance of "graphql" in the node_modules
directory. If different versions of "graphql" are the dependencies of other
relied on modules, use "resolutions" to ensure only one version is installed.

https://yarnpkg.com/en/docs/selective-version-resolutions

Duplicate "graphql" modules cannot be used at the same time since different
versions may have different capabilities and behavior. The data from one
version used in the function from another could produce confusing and
spurious results.`);
    }
  }
  return !1;
};
class je {
  constructor(t, n = "GraphQL request", s = {
    line: 1,
    column: 1
  }) {
    typeof t == "string" || te(!1, `Body must be a string. Received: ${xe(t)}.`), this.body = t, this.name = n, this.locationOffset = s, this.locationOffset.line > 0 || te(
      !1,
      "line in locationOffset is 1-indexed and must be positive."
    ), this.locationOffset.column > 0 || te(
      !1,
      "column in locationOffset is 1-indexed and must be positive."
    );
  }
  get [Symbol.toStringTag]() {
    return "Source";
  }
}
function Pt(e) {
  return Ft(e, je);
}
function Bt(e, t) {
  return new Mt(e, t).parseDocument();
}
class Mt {
  constructor(t, n = {}) {
    const s = Pt(t) ? t : new je(t);
    this._lexer = new xt(s), this._options = n, this._tokenCounter = 0;
  }
  parseName() {
    const t = this.expectToken(u.NAME);
    return this.node(t, {
      kind: y.NAME,
      value: t.value
    });
  }
  parseDocument() {
    return this.node(this._lexer.token, {
      kind: y.DOCUMENT,
      definitions: this.many(
        u.SOF,
        this.parseDefinition,
        u.EOF
      )
    });
  }
  parseDefinition() {
    if (this.peek(u.BRACE_L))
      return this.parseOperationDefinition();
    const t = this.peekDescription(), n = t ? this._lexer.lookahead() : this._lexer.token;
    if (n.kind === u.NAME) {
      switch (n.value) {
        case "schema":
          return this.parseSchemaDefinition();
        case "scalar":
          return this.parseScalarTypeDefinition();
        case "type":
          return this.parseObjectTypeDefinition();
        case "interface":
          return this.parseInterfaceTypeDefinition();
        case "union":
          return this.parseUnionTypeDefinition();
        case "enum":
          return this.parseEnumTypeDefinition();
        case "input":
          return this.parseInputObjectTypeDefinition();
        case "directive":
          return this.parseDirectiveDefinition();
      }
      if (t)
        throw C(
          this._lexer.source,
          this._lexer.token.start,
          "Unexpected description, descriptions are supported only on type definitions."
        );
      switch (n.value) {
        case "query":
        case "mutation":
        case "subscription":
          return this.parseOperationDefinition();
        case "fragment":
          return this.parseFragmentDefinition();
        case "extend":
          return this.parseTypeSystemExtension();
      }
    }
    throw this.unexpected(n);
  }
  parseOperationDefinition() {
    const t = this._lexer.token;
    if (this.peek(u.BRACE_L))
      return this.node(t, {
        kind: y.OPERATION_DEFINITION,
        operation: H.QUERY,
        name: void 0,
        variableDefinitions: [],
        directives: [],
        selectionSet: this.parseSelectionSet()
      });
    const n = this.parseOperationType();
    let s;
    return this.peek(u.NAME) && (s = this.parseName()), this.node(t, {
      kind: y.OPERATION_DEFINITION,
      operation: n,
      name: s,
      variableDefinitions: this.parseVariableDefinitions(),
      directives: this.parseDirectives(!1),
      selectionSet: this.parseSelectionSet()
    });
  }
  parseOperationType() {
    const t = this.expectToken(u.NAME);
    switch (t.value) {
      case "query":
        return H.QUERY;
      case "mutation":
        return H.MUTATION;
      case "subscription":
        return H.SUBSCRIPTION;
    }
    throw this.unexpected(t);
  }
  parseVariableDefinitions() {
    return this.optionalMany(
      u.PAREN_L,
      this.parseVariableDefinition,
      u.PAREN_R
    );
  }
  parseVariableDefinition() {
    return this.node(this._lexer.token, {
      kind: y.VARIABLE_DEFINITION,
      variable: this.parseVariable(),
      type: (this.expectToken(u.COLON), this.parseTypeReference()),
      defaultValue: this.expectOptionalToken(u.EQUALS) ? this.parseConstValueLiteral() : void 0,
      directives: this.parseConstDirectives()
    });
  }
  parseVariable() {
    const t = this._lexer.token;
    return this.expectToken(u.DOLLAR), this.node(t, {
      kind: y.VARIABLE,
      name: this.parseName()
    });
  }
  parseSelectionSet() {
    return this.node(this._lexer.token, {
      kind: y.SELECTION_SET,
      selections: this.many(
        u.BRACE_L,
        this.parseSelection,
        u.BRACE_R
      )
    });
  }
  parseSelection() {
    return this.peek(u.SPREAD) ? this.parseFragment() : this.parseField();
  }
  parseField() {
    const t = this._lexer.token, n = this.parseName();
    let s, r;
    return this.expectOptionalToken(u.COLON) ? (s = n, r = this.parseName()) : r = n, this.node(t, {
      kind: y.FIELD,
      alias: s,
      name: r,
      arguments: this.parseArguments(!1),
      directives: this.parseDirectives(!1),
      selectionSet: this.peek(u.BRACE_L) ? this.parseSelectionSet() : void 0
    });
  }
  parseArguments(t) {
    const n = t ? this.parseConstArgument : this.parseArgument;
    return this.optionalMany(u.PAREN_L, n, u.PAREN_R);
  }
  parseArgument(t = !1) {
    const n = this._lexer.token, s = this.parseName();
    return this.expectToken(u.COLON), this.node(n, {
      kind: y.ARGUMENT,
      name: s,
      value: this.parseValueLiteral(t)
    });
  }
  parseConstArgument() {
    return this.parseArgument(!0);
  }
  parseFragment() {
    const t = this._lexer.token;
    this.expectToken(u.SPREAD);
    const n = this.expectOptionalKeyword("on");
    return !n && this.peek(u.NAME) ? this.node(t, {
      kind: y.FRAGMENT_SPREAD,
      name: this.parseFragmentName(),
      directives: this.parseDirectives(!1)
    }) : this.node(t, {
      kind: y.INLINE_FRAGMENT,
      typeCondition: n ? this.parseNamedType() : void 0,
      directives: this.parseDirectives(!1),
      selectionSet: this.parseSelectionSet()
    });
  }
  parseFragmentDefinition() {
    const t = this._lexer.token;
    return this.expectKeyword("fragment"), this._options.allowLegacyFragmentVariables === !0 ? this.node(t, {
      kind: y.FRAGMENT_DEFINITION,
      name: this.parseFragmentName(),
      variableDefinitions: this.parseVariableDefinitions(),
      typeCondition: (this.expectKeyword("on"), this.parseNamedType()),
      directives: this.parseDirectives(!1),
      selectionSet: this.parseSelectionSet()
    }) : this.node(t, {
      kind: y.FRAGMENT_DEFINITION,
      name: this.parseFragmentName(),
      typeCondition: (this.expectKeyword("on"), this.parseNamedType()),
      directives: this.parseDirectives(!1),
      selectionSet: this.parseSelectionSet()
    });
  }
  parseFragmentName() {
    if (this._lexer.token.value === "on")
      throw this.unexpected();
    return this.parseName();
  }
  parseValueLiteral(t) {
    const n = this._lexer.token;
    switch (n.kind) {
      case u.BRACKET_L:
        return this.parseList(t);
      case u.BRACE_L:
        return this.parseObject(t);
      case u.INT:
        return this.advanceLexer(), this.node(n, {
          kind: y.INT,
          value: n.value
        });
      case u.FLOAT:
        return this.advanceLexer(), this.node(n, {
          kind: y.FLOAT,
          value: n.value
        });
      case u.STRING:
      case u.BLOCK_STRING:
        return this.parseStringLiteral();
      case u.NAME:
        switch (this.advanceLexer(), n.value) {
          case "true":
            return this.node(n, {
              kind: y.BOOLEAN,
              value: !0
            });
          case "false":
            return this.node(n, {
              kind: y.BOOLEAN,
              value: !1
            });
          case "null":
            return this.node(n, {
              kind: y.NULL
            });
          default:
            return this.node(n, {
              kind: y.ENUM,
              value: n.value
            });
        }
      case u.DOLLAR:
        if (t)
          if (this.expectToken(u.DOLLAR), this._lexer.token.kind === u.NAME) {
            const s = this._lexer.token.value;
            throw C(
              this._lexer.source,
              n.start,
              `Unexpected variable "$${s}" in constant value.`
            );
          } else
            throw this.unexpected(n);
        return this.parseVariable();
      default:
        throw this.unexpected();
    }
  }
  parseConstValueLiteral() {
    return this.parseValueLiteral(!0);
  }
  parseStringLiteral() {
    const t = this._lexer.token;
    return this.advanceLexer(), this.node(t, {
      kind: y.STRING,
      value: t.value,
      block: t.kind === u.BLOCK_STRING
    });
  }
  parseList(t) {
    const n = () => this.parseValueLiteral(t);
    return this.node(this._lexer.token, {
      kind: y.LIST,
      values: this.any(u.BRACKET_L, n, u.BRACKET_R)
    });
  }
  parseObject(t) {
    const n = () => this.parseObjectField(t);
    return this.node(this._lexer.token, {
      kind: y.OBJECT,
      fields: this.any(u.BRACE_L, n, u.BRACE_R)
    });
  }
  parseObjectField(t) {
    const n = this._lexer.token, s = this.parseName();
    return this.expectToken(u.COLON), this.node(n, {
      kind: y.OBJECT_FIELD,
      name: s,
      value: this.parseValueLiteral(t)
    });
  }
  parseDirectives(t) {
    const n = [];
    for (; this.peek(u.AT); )
      n.push(this.parseDirective(t));
    return n;
  }
  parseConstDirectives() {
    return this.parseDirectives(!0);
  }
  parseDirective(t) {
    const n = this._lexer.token;
    return this.expectToken(u.AT), this.node(n, {
      kind: y.DIRECTIVE,
      name: this.parseName(),
      arguments: this.parseArguments(t)
    });
  }
  parseTypeReference() {
    const t = this._lexer.token;
    let n;
    if (this.expectOptionalToken(u.BRACKET_L)) {
      const s = this.parseTypeReference();
      this.expectToken(u.BRACKET_R), n = this.node(t, {
        kind: y.LIST_TYPE,
        type: s
      });
    } else
      n = this.parseNamedType();
    return this.expectOptionalToken(u.BANG) ? this.node(t, {
      kind: y.NON_NULL_TYPE,
      type: n
    }) : n;
  }
  parseNamedType() {
    return this.node(this._lexer.token, {
      kind: y.NAMED_TYPE,
      name: this.parseName()
    });
  }
  peekDescription() {
    return this.peek(u.STRING) || this.peek(u.BLOCK_STRING);
  }
  parseDescription() {
    if (this.peekDescription())
      return this.parseStringLiteral();
  }
  parseSchemaDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("schema");
    const s = this.parseConstDirectives(), r = this.many(
      u.BRACE_L,
      this.parseOperationTypeDefinition,
      u.BRACE_R
    );
    return this.node(t, {
      kind: y.SCHEMA_DEFINITION,
      description: n,
      directives: s,
      operationTypes: r
    });
  }
  parseOperationTypeDefinition() {
    const t = this._lexer.token, n = this.parseOperationType();
    this.expectToken(u.COLON);
    const s = this.parseNamedType();
    return this.node(t, {
      kind: y.OPERATION_TYPE_DEFINITION,
      operation: n,
      type: s
    });
  }
  parseScalarTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("scalar");
    const s = this.parseName(), r = this.parseConstDirectives();
    return this.node(t, {
      kind: y.SCALAR_TYPE_DEFINITION,
      description: n,
      name: s,
      directives: r
    });
  }
  parseObjectTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("type");
    const s = this.parseName(), r = this.parseImplementsInterfaces(), i = this.parseConstDirectives(), o = this.parseFieldsDefinition();
    return this.node(t, {
      kind: y.OBJECT_TYPE_DEFINITION,
      description: n,
      name: s,
      interfaces: r,
      directives: i,
      fields: o
    });
  }
  parseImplementsInterfaces() {
    return this.expectOptionalKeyword("implements") ? this.delimitedMany(u.AMP, this.parseNamedType) : [];
  }
  parseFieldsDefinition() {
    return this.optionalMany(
      u.BRACE_L,
      this.parseFieldDefinition,
      u.BRACE_R
    );
  }
  parseFieldDefinition() {
    const t = this._lexer.token, n = this.parseDescription(), s = this.parseName(), r = this.parseArgumentDefs();
    this.expectToken(u.COLON);
    const i = this.parseTypeReference(), o = this.parseConstDirectives();
    return this.node(t, {
      kind: y.FIELD_DEFINITION,
      description: n,
      name: s,
      arguments: r,
      type: i,
      directives: o
    });
  }
  parseArgumentDefs() {
    return this.optionalMany(
      u.PAREN_L,
      this.parseInputValueDef,
      u.PAREN_R
    );
  }
  parseInputValueDef() {
    const t = this._lexer.token, n = this.parseDescription(), s = this.parseName();
    this.expectToken(u.COLON);
    const r = this.parseTypeReference();
    let i;
    this.expectOptionalToken(u.EQUALS) && (i = this.parseConstValueLiteral());
    const o = this.parseConstDirectives();
    return this.node(t, {
      kind: y.INPUT_VALUE_DEFINITION,
      description: n,
      name: s,
      type: r,
      defaultValue: i,
      directives: o
    });
  }
  parseInterfaceTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("interface");
    const s = this.parseName(), r = this.parseImplementsInterfaces(), i = this.parseConstDirectives(), o = this.parseFieldsDefinition();
    return this.node(t, {
      kind: y.INTERFACE_TYPE_DEFINITION,
      description: n,
      name: s,
      interfaces: r,
      directives: i,
      fields: o
    });
  }
  parseUnionTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("union");
    const s = this.parseName(), r = this.parseConstDirectives(), i = this.parseUnionMemberTypes();
    return this.node(t, {
      kind: y.UNION_TYPE_DEFINITION,
      description: n,
      name: s,
      directives: r,
      types: i
    });
  }
  parseUnionMemberTypes() {
    return this.expectOptionalToken(u.EQUALS) ? this.delimitedMany(u.PIPE, this.parseNamedType) : [];
  }
  parseEnumTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("enum");
    const s = this.parseName(), r = this.parseConstDirectives(), i = this.parseEnumValuesDefinition();
    return this.node(t, {
      kind: y.ENUM_TYPE_DEFINITION,
      description: n,
      name: s,
      directives: r,
      values: i
    });
  }
  parseEnumValuesDefinition() {
    return this.optionalMany(
      u.BRACE_L,
      this.parseEnumValueDefinition,
      u.BRACE_R
    );
  }
  parseEnumValueDefinition() {
    const t = this._lexer.token, n = this.parseDescription(), s = this.parseEnumValueName(), r = this.parseConstDirectives();
    return this.node(t, {
      kind: y.ENUM_VALUE_DEFINITION,
      description: n,
      name: s,
      directives: r
    });
  }
  parseEnumValueName() {
    if (this._lexer.token.value === "true" || this._lexer.token.value === "false" || this._lexer.token.value === "null")
      throw C(
        this._lexer.source,
        this._lexer.token.start,
        `${ee(
          this._lexer.token
        )} is reserved and cannot be used for an enum value.`
      );
    return this.parseName();
  }
  parseInputObjectTypeDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("input");
    const s = this.parseName(), r = this.parseConstDirectives(), i = this.parseInputFieldsDefinition();
    return this.node(t, {
      kind: y.INPUT_OBJECT_TYPE_DEFINITION,
      description: n,
      name: s,
      directives: r,
      fields: i
    });
  }
  parseInputFieldsDefinition() {
    return this.optionalMany(
      u.BRACE_L,
      this.parseInputValueDef,
      u.BRACE_R
    );
  }
  parseTypeSystemExtension() {
    const t = this._lexer.lookahead();
    if (t.kind === u.NAME)
      switch (t.value) {
        case "schema":
          return this.parseSchemaExtension();
        case "scalar":
          return this.parseScalarTypeExtension();
        case "type":
          return this.parseObjectTypeExtension();
        case "interface":
          return this.parseInterfaceTypeExtension();
        case "union":
          return this.parseUnionTypeExtension();
        case "enum":
          return this.parseEnumTypeExtension();
        case "input":
          return this.parseInputObjectTypeExtension();
      }
    throw this.unexpected(t);
  }
  parseSchemaExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("schema");
    const n = this.parseConstDirectives(), s = this.optionalMany(
      u.BRACE_L,
      this.parseOperationTypeDefinition,
      u.BRACE_R
    );
    if (n.length === 0 && s.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.SCHEMA_EXTENSION,
      directives: n,
      operationTypes: s
    });
  }
  parseScalarTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("scalar");
    const n = this.parseName(), s = this.parseConstDirectives();
    if (s.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.SCALAR_TYPE_EXTENSION,
      name: n,
      directives: s
    });
  }
  parseObjectTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("type");
    const n = this.parseName(), s = this.parseImplementsInterfaces(), r = this.parseConstDirectives(), i = this.parseFieldsDefinition();
    if (s.length === 0 && r.length === 0 && i.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.OBJECT_TYPE_EXTENSION,
      name: n,
      interfaces: s,
      directives: r,
      fields: i
    });
  }
  parseInterfaceTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("interface");
    const n = this.parseName(), s = this.parseImplementsInterfaces(), r = this.parseConstDirectives(), i = this.parseFieldsDefinition();
    if (s.length === 0 && r.length === 0 && i.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.INTERFACE_TYPE_EXTENSION,
      name: n,
      interfaces: s,
      directives: r,
      fields: i
    });
  }
  parseUnionTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("union");
    const n = this.parseName(), s = this.parseConstDirectives(), r = this.parseUnionMemberTypes();
    if (s.length === 0 && r.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.UNION_TYPE_EXTENSION,
      name: n,
      directives: s,
      types: r
    });
  }
  parseEnumTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("enum");
    const n = this.parseName(), s = this.parseConstDirectives(), r = this.parseEnumValuesDefinition();
    if (s.length === 0 && r.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.ENUM_TYPE_EXTENSION,
      name: n,
      directives: s,
      values: r
    });
  }
  parseInputObjectTypeExtension() {
    const t = this._lexer.token;
    this.expectKeyword("extend"), this.expectKeyword("input");
    const n = this.parseName(), s = this.parseConstDirectives(), r = this.parseInputFieldsDefinition();
    if (s.length === 0 && r.length === 0)
      throw this.unexpected();
    return this.node(t, {
      kind: y.INPUT_OBJECT_TYPE_EXTENSION,
      name: n,
      directives: s,
      fields: r
    });
  }
  parseDirectiveDefinition() {
    const t = this._lexer.token, n = this.parseDescription();
    this.expectKeyword("directive"), this.expectToken(u.AT);
    const s = this.parseName(), r = this.parseArgumentDefs(), i = this.expectOptionalKeyword("repeatable");
    this.expectKeyword("on");
    const o = this.parseDirectiveLocations();
    return this.node(t, {
      kind: y.DIRECTIVE_DEFINITION,
      description: n,
      name: s,
      arguments: r,
      repeatable: i,
      locations: o
    });
  }
  parseDirectiveLocations() {
    return this.delimitedMany(u.PIPE, this.parseDirectiveLocation);
  }
  parseDirectiveLocation() {
    const t = this._lexer.token, n = this.parseName();
    if (Object.prototype.hasOwnProperty.call(pe, n.value))
      return n;
    throw this.unexpected(t);
  }
  node(t, n) {
    return this._options.noLocation !== !0 && (n.loc = new ft(
      t,
      this._lexer.lastToken,
      this._lexer.source
    )), n;
  }
  peek(t) {
    return this._lexer.token.kind === t;
  }
  expectToken(t) {
    const n = this._lexer.token;
    if (n.kind === t)
      return this.advanceLexer(), n;
    throw C(
      this._lexer.source,
      n.start,
      `Expected ${Ve(t)}, found ${ee(n)}.`
    );
  }
  expectOptionalToken(t) {
    return this._lexer.token.kind === t ? (this.advanceLexer(), !0) : !1;
  }
  expectKeyword(t) {
    const n = this._lexer.token;
    if (n.kind === u.NAME && n.value === t)
      this.advanceLexer();
    else
      throw C(
        this._lexer.source,
        n.start,
        `Expected "${t}", found ${ee(n)}.`
      );
  }
  expectOptionalKeyword(t) {
    const n = this._lexer.token;
    return n.kind === u.NAME && n.value === t ? (this.advanceLexer(), !0) : !1;
  }
  unexpected(t) {
    const n = t != null ? t : this._lexer.token;
    return C(
      this._lexer.source,
      n.start,
      `Unexpected ${ee(n)}.`
    );
  }
  any(t, n, s) {
    this.expectToken(t);
    const r = [];
    for (; !this.expectOptionalToken(s); )
      r.push(n.call(this));
    return r;
  }
  optionalMany(t, n, s) {
    if (this.expectOptionalToken(t)) {
      const r = [];
      do
        r.push(n.call(this));
      while (!this.expectOptionalToken(s));
      return r;
    }
    return [];
  }
  many(t, n, s) {
    this.expectToken(t);
    const r = [];
    do
      r.push(n.call(this));
    while (!this.expectOptionalToken(s));
    return r;
  }
  delimitedMany(t, n) {
    this.expectOptionalToken(t);
    const s = [];
    do
      s.push(n.call(this));
    while (this.expectOptionalToken(t));
    return s;
  }
  advanceLexer() {
    const { maxTokens: t } = this._options, n = this._lexer.advance();
    if (t !== void 0 && n.kind !== u.EOF && (++this._tokenCounter, this._tokenCounter > t))
      throw C(
        this._lexer.source,
        n.start,
        `Document contains more that ${t} tokens. Parsing aborted.`
      );
  }
}
function ee(e) {
  const t = e.value;
  return Ve(e.kind) + (t != null ? ` "${t}"` : "");
}
function Ve(e) {
  return vt(e) ? `"${e}"` : e;
}
function jt(e) {
  return `"${e.replace(Vt, qt)}"`;
}
const Vt = /[\x00-\x1f\x22\x5c\x7f-\x9f]/g;
function qt(e) {
  return Ht[e.charCodeAt(0)];
}
const Ht = [
  "\\u0000",
  "\\u0001",
  "\\u0002",
  "\\u0003",
  "\\u0004",
  "\\u0005",
  "\\u0006",
  "\\u0007",
  "\\b",
  "\\t",
  "\\n",
  "\\u000B",
  "\\f",
  "\\r",
  "\\u000E",
  "\\u000F",
  "\\u0010",
  "\\u0011",
  "\\u0012",
  "\\u0013",
  "\\u0014",
  "\\u0015",
  "\\u0016",
  "\\u0017",
  "\\u0018",
  "\\u0019",
  "\\u001A",
  "\\u001B",
  "\\u001C",
  "\\u001D",
  "\\u001E",
  "\\u001F",
  "",
  "",
  '\\"',
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "\\\\",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "\\u007F",
  "\\u0080",
  "\\u0081",
  "\\u0082",
  "\\u0083",
  "\\u0084",
  "\\u0085",
  "\\u0086",
  "\\u0087",
  "\\u0088",
  "\\u0089",
  "\\u008A",
  "\\u008B",
  "\\u008C",
  "\\u008D",
  "\\u008E",
  "\\u008F",
  "\\u0090",
  "\\u0091",
  "\\u0092",
  "\\u0093",
  "\\u0094",
  "\\u0095",
  "\\u0096",
  "\\u0097",
  "\\u0098",
  "\\u0099",
  "\\u009A",
  "\\u009B",
  "\\u009C",
  "\\u009D",
  "\\u009E",
  "\\u009F"
], Qt = Object.freeze({});
function $t(e, t, n = Ue) {
  const s = /* @__PURE__ */ new Map();
  for (const R of Object.values(y))
    s.set(R, Gt(t, R));
  let r, i = Array.isArray(e), o = [e], c = -1, d = [], l = e, h, T;
  const E = [], b = [];
  do {
    c++;
    const R = c === o.length, j = R && d.length !== 0;
    if (R) {
      if (h = b.length === 0 ? void 0 : E[E.length - 1], l = T, T = b.pop(), j)
        if (i) {
          l = l.slice();
          let S = 0;
          for (const [V, W] of d) {
            const K = V - S;
            W === null ? (l.splice(K, 1), S++) : l[K] = W;
          }
        } else {
          l = Object.defineProperties(
            {},
            Object.getOwnPropertyDescriptors(l)
          );
          for (const [S, V] of d)
            l[S] = V;
        }
      c = r.index, o = r.keys, d = r.edits, i = r.inArray, r = r.prev;
    } else if (T) {
      if (h = i ? c : o[c], l = T[h], l == null)
        continue;
      E.push(h);
    }
    let U;
    if (!Array.isArray(l)) {
      var m, A;
      Ce(l) || te(!1, `Invalid AST Node: ${xe(l)}.`);
      const S = R ? (m = s.get(l.kind)) === null || m === void 0 ? void 0 : m.leave : (A = s.get(l.kind)) === null || A === void 0 ? void 0 : A.enter;
      if (U = S == null ? void 0 : S.call(t, l, h, T, E, b), U === Qt)
        break;
      if (U === !1) {
        if (!R) {
          E.pop();
          continue;
        }
      } else if (U !== void 0 && (d.push([h, U]), !R))
        if (Ce(U))
          l = U;
        else {
          E.pop();
          continue;
        }
    }
    if (U === void 0 && j && d.push([h, l]), R)
      E.pop();
    else {
      var w;
      r = {
        inArray: i,
        index: c,
        keys: o,
        edits: d,
        prev: r
      }, i = Array.isArray(l), o = i ? l : (w = n[l.kind]) !== null && w !== void 0 ? w : [], c = -1, d = [], T && b.push(T), T = l;
    }
  } while (r !== void 0);
  return d.length !== 0 ? d[d.length - 1][1] : e;
}
function Gt(e, t) {
  const n = e[t];
  return typeof n == "object" ? n : typeof n == "function" ? {
    enter: n,
    leave: void 0
  } : {
    enter: e.enter,
    leave: e.leave
  };
}
function zt(e) {
  return $t(e, Xt);
}
const Yt = 80, Xt = {
  Name: {
    leave: (e) => e.value
  },
  Variable: {
    leave: (e) => "$" + e.name
  },
  Document: {
    leave: (e) => p(e.definitions, `

`)
  },
  OperationDefinition: {
    leave(e) {
      const t = O("(", p(e.variableDefinitions, ", "), ")"), n = p(
        [
          e.operation,
          p([e.name, t]),
          p(e.directives, " ")
        ],
        " "
      );
      return (n === "query" ? "" : n + " ") + e.selectionSet;
    }
  },
  VariableDefinition: {
    leave: ({ variable: e, type: t, defaultValue: n, directives: s }) => e + ": " + t + O(" = ", n) + O(" ", p(s, " "))
  },
  SelectionSet: {
    leave: ({ selections: e }) => D(e)
  },
  Field: {
    leave({ alias: e, name: t, arguments: n, directives: s, selectionSet: r }) {
      const i = O("", e, ": ") + t;
      let o = i + O("(", p(n, ", "), ")");
      return o.length > Yt && (o = i + O(`(
`, ne(p(n, `
`)), `
)`)), p([o, p(s, " "), r], " ");
    }
  },
  Argument: {
    leave: ({ name: e, value: t }) => e + ": " + t
  },
  FragmentSpread: {
    leave: ({ name: e, directives: t }) => "..." + e + O(" ", p(t, " "))
  },
  InlineFragment: {
    leave: ({ typeCondition: e, directives: t, selectionSet: n }) => p(
      [
        "...",
        O("on ", e),
        p(t, " "),
        n
      ],
      " "
    )
  },
  FragmentDefinition: {
    leave: ({ name: e, typeCondition: t, variableDefinitions: n, directives: s, selectionSet: r }) => `fragment ${e}${O("(", p(n, ", "), ")")} on ${t} ${O("", p(s, " "), " ")}` + r
  },
  IntValue: {
    leave: ({ value: e }) => e
  },
  FloatValue: {
    leave: ({ value: e }) => e
  },
  StringValue: {
    leave: ({ value: e, block: t }) => t ? Et(e) : jt(e)
  },
  BooleanValue: {
    leave: ({ value: e }) => e ? "true" : "false"
  },
  NullValue: {
    leave: () => "null"
  },
  EnumValue: {
    leave: ({ value: e }) => e
  },
  ListValue: {
    leave: ({ values: e }) => "[" + p(e, ", ") + "]"
  },
  ObjectValue: {
    leave: ({ fields: e }) => "{" + p(e, ", ") + "}"
  },
  ObjectField: {
    leave: ({ name: e, value: t }) => e + ": " + t
  },
  Directive: {
    leave: ({ name: e, arguments: t }) => "@" + e + O("(", p(t, ", "), ")")
  },
  NamedType: {
    leave: ({ name: e }) => e
  },
  ListType: {
    leave: ({ type: e }) => "[" + e + "]"
  },
  NonNullType: {
    leave: ({ type: e }) => e + "!"
  },
  SchemaDefinition: {
    leave: ({ description: e, directives: t, operationTypes: n }) => O("", e, `
`) + p(["schema", p(t, " "), D(n)], " ")
  },
  OperationTypeDefinition: {
    leave: ({ operation: e, type: t }) => e + ": " + t
  },
  ScalarTypeDefinition: {
    leave: ({ description: e, name: t, directives: n }) => O("", e, `
`) + p(["scalar", t, p(n, " ")], " ")
  },
  ObjectTypeDefinition: {
    leave: ({ description: e, name: t, interfaces: n, directives: s, fields: r }) => O("", e, `
`) + p(
      [
        "type",
        t,
        O("implements ", p(n, " & ")),
        p(s, " "),
        D(r)
      ],
      " "
    )
  },
  FieldDefinition: {
    leave: ({ description: e, name: t, arguments: n, type: s, directives: r }) => O("", e, `
`) + t + (Ne(n) ? O(`(
`, ne(p(n, `
`)), `
)`) : O("(", p(n, ", "), ")")) + ": " + s + O(" ", p(r, " "))
  },
  InputValueDefinition: {
    leave: ({ description: e, name: t, type: n, defaultValue: s, directives: r }) => O("", e, `
`) + p(
      [t + ": " + n, O("= ", s), p(r, " ")],
      " "
    )
  },
  InterfaceTypeDefinition: {
    leave: ({ description: e, name: t, interfaces: n, directives: s, fields: r }) => O("", e, `
`) + p(
      [
        "interface",
        t,
        O("implements ", p(n, " & ")),
        p(s, " "),
        D(r)
      ],
      " "
    )
  },
  UnionTypeDefinition: {
    leave: ({ description: e, name: t, directives: n, types: s }) => O("", e, `
`) + p(
      ["union", t, p(n, " "), O("= ", p(s, " | "))],
      " "
    )
  },
  EnumTypeDefinition: {
    leave: ({ description: e, name: t, directives: n, values: s }) => O("", e, `
`) + p(["enum", t, p(n, " "), D(s)], " ")
  },
  EnumValueDefinition: {
    leave: ({ description: e, name: t, directives: n }) => O("", e, `
`) + p([t, p(n, " ")], " ")
  },
  InputObjectTypeDefinition: {
    leave: ({ description: e, name: t, directives: n, fields: s }) => O("", e, `
`) + p(["input", t, p(n, " "), D(s)], " ")
  },
  DirectiveDefinition: {
    leave: ({ description: e, name: t, arguments: n, repeatable: s, locations: r }) => O("", e, `
`) + "directive @" + t + (Ne(n) ? O(`(
`, ne(p(n, `
`)), `
)`) : O("(", p(n, ", "), ")")) + (s ? " repeatable" : "") + " on " + p(r, " | ")
  },
  SchemaExtension: {
    leave: ({ directives: e, operationTypes: t }) => p(
      ["extend schema", p(e, " "), D(t)],
      " "
    )
  },
  ScalarTypeExtension: {
    leave: ({ name: e, directives: t }) => p(["extend scalar", e, p(t, " ")], " ")
  },
  ObjectTypeExtension: {
    leave: ({ name: e, interfaces: t, directives: n, fields: s }) => p(
      [
        "extend type",
        e,
        O("implements ", p(t, " & ")),
        p(n, " "),
        D(s)
      ],
      " "
    )
  },
  InterfaceTypeExtension: {
    leave: ({ name: e, interfaces: t, directives: n, fields: s }) => p(
      [
        "extend interface",
        e,
        O("implements ", p(t, " & ")),
        p(n, " "),
        D(s)
      ],
      " "
    )
  },
  UnionTypeExtension: {
    leave: ({ name: e, directives: t, types: n }) => p(
      [
        "extend union",
        e,
        p(t, " "),
        O("= ", p(n, " | "))
      ],
      " "
    )
  },
  EnumTypeExtension: {
    leave: ({ name: e, directives: t, values: n }) => p(["extend enum", e, p(t, " "), D(n)], " ")
  },
  InputObjectTypeExtension: {
    leave: ({ name: e, directives: t, fields: n }) => p(["extend input", e, p(t, " "), D(n)], " ")
  }
};
function p(e, t = "") {
  var n;
  return (n = e == null ? void 0 : e.filter((s) => s).join(t)) !== null && n !== void 0 ? n : "";
}
function D(e) {
  return O(`{
`, ne(p(e, `
`)), `
}`);
}
function O(e, t, n = "") {
  return t != null && t !== "" ? e + t + n : "";
}
function ne(e) {
  return O("  ", e.replace(/\n/g, `
  `));
}
function Ne(e) {
  var t;
  return (t = e == null ? void 0 : e.some((n) => n.includes(`
`))) !== null && t !== void 0 ? t : !1;
}
const we = (e) => {
  var s, r;
  let t;
  const n = e.definitions.filter((i) => i.kind === "OperationDefinition");
  return n.length === 1 && (t = (r = (s = n[0]) == null ? void 0 : s.name) == null ? void 0 : r.value), t;
}, de = (e) => {
  if (typeof e == "string") {
    let n;
    try {
      const s = Bt(e);
      n = we(s);
    } catch {
    }
    return { query: e, operationName: n };
  }
  const t = we(e);
  return { query: zt(e), operationName: t };
};
class Y extends Error {
  constructor(t, n) {
    const s = `${Y.extractMessage(t)}: ${JSON.stringify({
      response: t,
      request: n
    })}`;
    super(s), Object.setPrototypeOf(this, Y.prototype), this.response = t, this.request = n, typeof Error.captureStackTrace == "function" && Error.captureStackTrace(this, Y);
  }
  static extractMessage(t) {
    var n, s, r;
    return (r = (s = (n = t.errors) == null ? void 0 : n[0]) == null ? void 0 : s.message) != null ? r : `GraphQL Error (Code: ${t.status})`;
  }
}
var Jt = typeof globalThis < "u" ? globalThis : typeof window < "u" ? window : typeof global < "u" ? global : typeof self < "u" ? self : {};
function Wt(e) {
  return e && e.__esModule && Object.prototype.hasOwnProperty.call(e, "default") ? e.default : e;
}
var Q = { exports: {} };
(function(e, t) {
  var n = typeof self < "u" ? self : Jt, s = function() {
    function i() {
      this.fetch = !1, this.DOMException = n.DOMException;
    }
    return i.prototype = n, new i();
  }();
  (function(i) {
    (function(o) {
      var c = {
        searchParams: "URLSearchParams" in i,
        iterable: "Symbol" in i && "iterator" in Symbol,
        blob: "FileReader" in i && "Blob" in i && function() {
          try {
            return new Blob(), !0;
          } catch {
            return !1;
          }
        }(),
        formData: "FormData" in i,
        arrayBuffer: "ArrayBuffer" in i
      };
      function d(a) {
        return a && DataView.prototype.isPrototypeOf(a);
      }
      if (c.arrayBuffer)
        var l = [
          "[object Int8Array]",
          "[object Uint8Array]",
          "[object Uint8ClampedArray]",
          "[object Int16Array]",
          "[object Uint16Array]",
          "[object Int32Array]",
          "[object Uint32Array]",
          "[object Float32Array]",
          "[object Float64Array]"
        ], h = ArrayBuffer.isView || function(a) {
          return a && l.indexOf(Object.prototype.toString.call(a)) > -1;
        };
      function T(a) {
        if (typeof a != "string" && (a = String(a)), /[^a-z0-9\-#$%&'*+.^_`|~]/i.test(a))
          throw new TypeError("Invalid character in header field name");
        return a.toLowerCase();
      }
      function E(a) {
        return typeof a != "string" && (a = String(a)), a;
      }
      function b(a) {
        var f = {
          next: function() {
            var v = a.shift();
            return { done: v === void 0, value: v };
          }
        };
        return c.iterable && (f[Symbol.iterator] = function() {
          return f;
        }), f;
      }
      function m(a) {
        this.map = {}, a instanceof m ? a.forEach(function(f, v) {
          this.append(v, f);
        }, this) : Array.isArray(a) ? a.forEach(function(f) {
          this.append(f[0], f[1]);
        }, this) : a && Object.getOwnPropertyNames(a).forEach(function(f) {
          this.append(f, a[f]);
        }, this);
      }
      m.prototype.append = function(a, f) {
        a = T(a), f = E(f);
        var v = this.map[a];
        this.map[a] = v ? v + ", " + f : f;
      }, m.prototype.delete = function(a) {
        delete this.map[T(a)];
      }, m.prototype.get = function(a) {
        return a = T(a), this.has(a) ? this.map[a] : null;
      }, m.prototype.has = function(a) {
        return this.map.hasOwnProperty(T(a));
      }, m.prototype.set = function(a, f) {
        this.map[T(a)] = E(f);
      }, m.prototype.forEach = function(a, f) {
        for (var v in this.map)
          this.map.hasOwnProperty(v) && a.call(f, this.map[v], v, this);
      }, m.prototype.keys = function() {
        var a = [];
        return this.forEach(function(f, v) {
          a.push(v);
        }), b(a);
      }, m.prototype.values = function() {
        var a = [];
        return this.forEach(function(f) {
          a.push(f);
        }), b(a);
      }, m.prototype.entries = function() {
        var a = [];
        return this.forEach(function(f, v) {
          a.push([v, f]);
        }), b(a);
      }, c.iterable && (m.prototype[Symbol.iterator] = m.prototype.entries);
      function A(a) {
        if (a.bodyUsed)
          return Promise.reject(new TypeError("Already read"));
        a.bodyUsed = !0;
      }
      function w(a) {
        return new Promise(function(f, v) {
          a.onload = function() {
            f(a.result);
          }, a.onerror = function() {
            v(a.error);
          };
        });
      }
      function R(a) {
        var f = new FileReader(), v = w(f);
        return f.readAsArrayBuffer(a), v;
      }
      function j(a) {
        var f = new FileReader(), v = w(f);
        return f.readAsText(a), v;
      }
      function U(a) {
        for (var f = new Uint8Array(a), v = new Array(f.length), N = 0; N < f.length; N++)
          v[N] = String.fromCharCode(f[N]);
        return v.join("");
      }
      function S(a) {
        if (a.slice)
          return a.slice(0);
        var f = new Uint8Array(a.byteLength);
        return f.set(new Uint8Array(a)), f.buffer;
      }
      function V() {
        return this.bodyUsed = !1, this._initBody = function(a) {
          this._bodyInit = a, a ? typeof a == "string" ? this._bodyText = a : c.blob && Blob.prototype.isPrototypeOf(a) ? this._bodyBlob = a : c.formData && FormData.prototype.isPrototypeOf(a) ? this._bodyFormData = a : c.searchParams && URLSearchParams.prototype.isPrototypeOf(a) ? this._bodyText = a.toString() : c.arrayBuffer && c.blob && d(a) ? (this._bodyArrayBuffer = S(a.buffer), this._bodyInit = new Blob([this._bodyArrayBuffer])) : c.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(a) || h(a)) ? this._bodyArrayBuffer = S(a) : this._bodyText = a = Object.prototype.toString.call(a) : this._bodyText = "", this.headers.get("content-type") || (typeof a == "string" ? this.headers.set("content-type", "text/plain;charset=UTF-8") : this._bodyBlob && this._bodyBlob.type ? this.headers.set("content-type", this._bodyBlob.type) : c.searchParams && URLSearchParams.prototype.isPrototypeOf(a) && this.headers.set("content-type", "application/x-www-form-urlencoded;charset=UTF-8"));
        }, c.blob && (this.blob = function() {
          var a = A(this);
          if (a)
            return a;
          if (this._bodyBlob)
            return Promise.resolve(this._bodyBlob);
          if (this._bodyArrayBuffer)
            return Promise.resolve(new Blob([this._bodyArrayBuffer]));
          if (this._bodyFormData)
            throw new Error("could not read FormData body as blob");
          return Promise.resolve(new Blob([this._bodyText]));
        }, this.arrayBuffer = function() {
          return this._bodyArrayBuffer ? A(this) || Promise.resolve(this._bodyArrayBuffer) : this.blob().then(R);
        }), this.text = function() {
          var a = A(this);
          if (a)
            return a;
          if (this._bodyBlob)
            return j(this._bodyBlob);
          if (this._bodyArrayBuffer)
            return Promise.resolve(U(this._bodyArrayBuffer));
          if (this._bodyFormData)
            throw new Error("could not read FormData body as text");
          return Promise.resolve(this._bodyText);
        }, c.formData && (this.formData = function() {
          return this.text().then(Ye);
        }), this.json = function() {
          return this.text().then(JSON.parse);
        }, this;
      }
      var W = ["DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT"];
      function K(a) {
        var f = a.toUpperCase();
        return W.indexOf(f) > -1 ? f : a;
      }
      function B(a, f) {
        f = f || {};
        var v = f.body;
        if (a instanceof B) {
          if (a.bodyUsed)
            throw new TypeError("Already read");
          this.url = a.url, this.credentials = a.credentials, f.headers || (this.headers = new m(a.headers)), this.method = a.method, this.mode = a.mode, this.signal = a.signal, !v && a._bodyInit != null && (v = a._bodyInit, a.bodyUsed = !0);
        } else
          this.url = String(a);
        if (this.credentials = f.credentials || this.credentials || "same-origin", (f.headers || !this.headers) && (this.headers = new m(f.headers)), this.method = K(f.method || this.method || "GET"), this.mode = f.mode || this.mode || null, this.signal = f.signal || this.signal, this.referrer = null, (this.method === "GET" || this.method === "HEAD") && v)
          throw new TypeError("Body not allowed for GET or HEAD requests");
        this._initBody(v);
      }
      B.prototype.clone = function() {
        return new B(this, { body: this._bodyInit });
      };
      function Ye(a) {
        var f = new FormData();
        return a.trim().split("&").forEach(function(v) {
          if (v) {
            var N = v.split("="), I = N.shift().replace(/\+/g, " "), g = N.join("=").replace(/\+/g, " ");
            f.append(decodeURIComponent(I), decodeURIComponent(g));
          }
        }), f;
      }
      function Xe(a) {
        var f = new m(), v = a.replace(/\r?\n[\t ]+/g, " ");
        return v.split(/\r?\n/).forEach(function(N) {
          var I = N.split(":"), g = I.shift().trim();
          if (g) {
            var Z = I.join(":").trim();
            f.append(g, Z);
          }
        }), f;
      }
      V.call(B.prototype);
      function L(a, f) {
        f || (f = {}), this.type = "default", this.status = f.status === void 0 ? 200 : f.status, this.ok = this.status >= 200 && this.status < 300, this.statusText = "statusText" in f ? f.statusText : "OK", this.headers = new m(f.headers), this.url = f.url || "", this._initBody(a);
      }
      V.call(L.prototype), L.prototype.clone = function() {
        return new L(this._bodyInit, {
          status: this.status,
          statusText: this.statusText,
          headers: new m(this.headers),
          url: this.url
        });
      }, L.error = function() {
        var a = new L(null, { status: 0, statusText: "" });
        return a.type = "error", a;
      };
      var Je = [301, 302, 303, 307, 308];
      L.redirect = function(a, f) {
        if (Je.indexOf(f) === -1)
          throw new RangeError("Invalid status code");
        return new L(null, { status: f, headers: { location: a } });
      }, o.DOMException = i.DOMException;
      try {
        new o.DOMException();
      } catch {
        o.DOMException = function(f, v) {
          this.message = f, this.name = v;
          var N = Error(f);
          this.stack = N.stack;
        }, o.DOMException.prototype = Object.create(Error.prototype), o.DOMException.prototype.constructor = o.DOMException;
      }
      function ae(a, f) {
        return new Promise(function(v, N) {
          var I = new B(a, f);
          if (I.signal && I.signal.aborted)
            return N(new o.DOMException("Aborted", "AbortError"));
          var g = new XMLHttpRequest();
          function Z() {
            g.abort();
          }
          g.onload = function() {
            var G = {
              status: g.status,
              statusText: g.statusText,
              headers: Xe(g.getAllResponseHeaders() || "")
            };
            G.url = "responseURL" in g ? g.responseURL : G.headers.get("X-Request-URL");
            var ce = "response" in g ? g.response : g.responseText;
            v(new L(ce, G));
          }, g.onerror = function() {
            N(new TypeError("Network request failed"));
          }, g.ontimeout = function() {
            N(new TypeError("Network request failed"));
          }, g.onabort = function() {
            N(new o.DOMException("Aborted", "AbortError"));
          }, g.open(I.method, I.url, !0), I.credentials === "include" ? g.withCredentials = !0 : I.credentials === "omit" && (g.withCredentials = !1), "responseType" in g && c.blob && (g.responseType = "blob"), I.headers.forEach(function(G, ce) {
            g.setRequestHeader(ce, G);
          }), I.signal && (I.signal.addEventListener("abort", Z), g.onreadystatechange = function() {
            g.readyState === 4 && I.signal.removeEventListener("abort", Z);
          }), g.send(typeof I._bodyInit > "u" ? null : I._bodyInit);
        });
      }
      return ae.polyfill = !0, i.fetch || (i.fetch = ae, i.Headers = m, i.Request = B, i.Response = L), o.Headers = m, o.Request = B, o.Response = L, o.fetch = ae, Object.defineProperty(o, "__esModule", { value: !0 }), o;
    })({});
  })(s), s.fetch.ponyfill = !0, delete s.fetch.polyfill;
  var r = s;
  t = r.fetch, t.default = r.fetch, t.fetch = r.fetch, t.Headers = r.Headers, t.Request = r.Request, t.Response = r.Response, e.exports = t;
})(Q, Q.exports);
const se = /* @__PURE__ */ Wt(Q.exports), Kt = /* @__PURE__ */ tt({
  __proto__: null,
  default: se
}, [Q.exports]), q = (e) => {
  let t = {};
  return e && (typeof Headers < "u" && e instanceof Headers || Kt && Q.exports.Headers && e instanceof Q.exports.Headers ? t = rt(e) : Array.isArray(e) ? e.forEach(([n, s]) => {
    n && s !== void 0 && (t[n] = s);
  }) : t = e), t;
}, ke = (e) => e.replace(/([\s,]|#[^\n\r]+)+/g, " ").trim(), Zt = (e) => {
  if (!Array.isArray(e.query)) {
    const s = e, r = [`query=${encodeURIComponent(ke(s.query))}`];
    return e.variables && r.push(`variables=${encodeURIComponent(s.jsonSerializer.stringify(s.variables))}`), s.operationName && r.push(`operationName=${encodeURIComponent(s.operationName)}`), r.join("&");
  }
  if (typeof e.variables < "u" && !Array.isArray(e.variables))
    throw new Error("Cannot create query with given variable type, array expected");
  const t = e, n = e.query.reduce((s, r, i) => (s.push({
    query: ke(r),
    variables: t.variables ? t.jsonSerializer.stringify(t.variables[i]) : void 0
  }), s), []);
  return `query=${encodeURIComponent(t.jsonSerializer.stringify(n))}`;
}, en = (e) => async (t) => {
  var A;
  const { url: n, query: s, variables: r, operationName: i, fetch: o, fetchOptions: c, middleware: d } = t, l = { ...t.headers };
  let h = "", T;
  e === "POST" ? (T = nn(s, r, i, c.jsonSerializer), typeof T == "string" && (l["Content-Type"] = "application/json")) : h = Zt({
    query: s,
    variables: r,
    operationName: i,
    jsonSerializer: (A = c.jsonSerializer) != null ? A : ye
  });
  const E = {
    method: e,
    headers: l,
    body: T,
    ...c
  };
  let b = n, m = E;
  if (d) {
    const w = await Promise.resolve(d({ ...E, url: n, operationName: i, variables: r })), { url: R, ...j } = w;
    b = R, m = j;
  }
  return h && (b = `${b}?${h}`), await o(b, m);
};
class tn {
  constructor(t, n = {}) {
    this.url = t, this.requestConfig = n, this.rawRequest = async (...s) => {
      const [r, i, o] = s, c = ot(r, i, o), { headers: d, fetch: l = se, method: h = "POST", requestMiddleware: T, responseMiddleware: E, ...b } = this.requestConfig, { url: m } = this;
      c.signal !== void 0 && (b.signal = c.signal);
      const { operationName: A } = de(c.query);
      return le({
        url: m,
        query: c.query,
        variables: c.variables,
        headers: {
          ...q(he(d)),
          ...q(c.requestHeaders)
        },
        operationName: A,
        fetch: l,
        method: h,
        fetchOptions: b,
        middleware: T
      }).then((w) => (E && E(w), w)).catch((w) => {
        throw E && E(w), w;
      });
    };
  }
  async request(t, ...n) {
    const [s, r] = n, i = it(t, s, r), { headers: o, fetch: c = se, method: d = "POST", requestMiddleware: l, responseMiddleware: h, ...T } = this.requestConfig, { url: E } = this;
    i.signal !== void 0 && (T.signal = i.signal);
    const { query: b, operationName: m } = de(i.document);
    return le({
      url: E,
      query: b,
      variables: i.variables,
      headers: {
        ...q(he(o)),
        ...q(i.requestHeaders)
      },
      operationName: m,
      fetch: c,
      method: d,
      fetchOptions: T,
      middleware: l
    }).then((A) => (h && h(A), A.data)).catch((A) => {
      throw h && h(A), A;
    });
  }
  batchRequests(t, n) {
    var d;
    const s = at(t, n), { headers: r, ...i } = this.requestConfig;
    s.signal !== void 0 && (i.signal = s.signal);
    const o = s.documents.map(({ document: l }) => de(l).query), c = s.documents.map(({ variables: l }) => l);
    return le({
      url: this.url,
      query: o,
      variables: c,
      headers: {
        ...q(he(r)),
        ...q(s.requestHeaders)
      },
      operationName: void 0,
      fetch: (d = this.requestConfig.fetch) != null ? d : se,
      method: this.requestConfig.method || "POST",
      fetchOptions: i,
      middleware: this.requestConfig.requestMiddleware
    }).then((l) => (this.requestConfig.responseMiddleware && this.requestConfig.responseMiddleware(l), l.data)).catch((l) => {
      throw this.requestConfig.responseMiddleware && this.requestConfig.responseMiddleware(l), l;
    });
  }
  setHeaders(t) {
    return this.requestConfig.headers = t, this;
  }
  setHeader(t, n) {
    const { headers: s } = this.requestConfig;
    return s ? s[t] = n : this.requestConfig.headers = { [t]: n }, this;
  }
  setEndpoint(t) {
    return this.url = t, this;
  }
}
const le = async (e) => {
  var h, T;
  const { query: t, variables: n, fetchOptions: s } = e, r = en(st((h = e.method) != null ? h : "post")), i = Array.isArray(e.query), o = await r(e), c = await sn(o, (T = s.jsonSerializer) != null ? T : ye), d = Array.isArray(c) ? !c.some(({ data: E }) => !E) : Boolean(c.data), l = Array.isArray(c) || !c.errors || Array.isArray(c.errors) && !c.errors.length || s.errorPolicy === "all" || s.errorPolicy === "ignore";
  if (o.ok && l && d) {
    const { errors: E, ...b } = (Array.isArray(c), c), m = s.errorPolicy === "ignore" ? b : c;
    return {
      ...i ? { data: m } : m,
      headers: o.headers,
      status: o.status
    };
  } else {
    const E = typeof c == "string" ? {
      error: c
    } : c;
    throw new Y(
      { ...E, status: o.status, headers: o.headers },
      { query: t, variables: n }
    );
  }
}, nn = (e, t, n, s) => {
  const r = s != null ? s : ye;
  if (!Array.isArray(e))
    return r.stringify({ query: e, variables: t, operationName: n });
  if (typeof t < "u" && !Array.isArray(t))
    throw new Error("Cannot create request body with given variable type, array expected");
  const i = e.reduce((o, c, d) => (o.push({ query: c, variables: t ? t[d] : void 0 }), o), []);
  return r.stringify(i);
}, sn = async (e, t) => {
  let n;
  return e.headers.forEach((s, r) => {
    r.toLowerCase() === "content-type" && (n = s);
  }), n && (n.toLowerCase().startsWith("application/json") || n.toLowerCase().startsWith("application/graphql+json") || n.toLowerCase().startsWith("application/graphql-response+json")) ? t.parse(await e.text()) : e.text();
}, he = (e) => typeof e == "function" ? e() : e, oe = (e, ...t) => e.reduce((n, s, r) => `${n}${s}${r in t ? String(t[r]) : ""}`, "");
class k {
  constructor({ andamioConfig: t }) {
    x(this, "graphQLClient");
    x(this, "andamioConfig");
    x(this, "graphQlUrl");
    this.graphQlUrl = "https://d.graphql-api.iohk-preprod.dandelion.link/", this.graphQLClient = new tn(this.graphQlUrl, {
      headers: {
        "Content-Type": "application/json"
      }
    }), this.andamioConfig = t;
  }
}
const F = oe`
  query GetContractUTxOs($contractAddress: String!) {
    utxos(where: { address: { _eq: $contractAddress } }) {
      txHash
      index
      address
      value
      tokens {
        asset {
          policyId
          assetName
          assetId
          fingerprint
        }
        quantity
      }
      datum {
        bytes
        value
      }
    }
  }
`;
function rn(e) {
  const t = [];
  return e.forEach((n) => {
    if (n.datum) {
      const s = [{ unit: "lovelace", quantity: n.value }];
      n.tokens.forEach((o) => {
        s.push({ unit: o.asset.assetId, quantity: o.quantity });
      });
      const r = {
        input: {
          txHash: n.txHash,
          outputIndex: n.index
        },
        output: {
          address: n.address,
          amount: s,
          plutusData: n.datum.bytes
        }
      };
      let i;
      if (n.datum.value.fields[0].fields[1].constructor == 1 ? i = {
        alternative: 0,
        fields: [
          {
            alternative: 0,
            fields: [n.datum.value.fields[0].fields[0]]
          },
          { alternative: 1, fields: [] }
        ]
      } : n.datum.value.fields[0].fields[1].constructor == 0 && (i = {
        alternative: 0,
        fields: [
          {
            alternative: 0,
            fields: [n.datum.value.fields[0].fields[0]]
          },
          {
            alternative: 0,
            fields: [
              {
                fields: [
                  {
                    fields: [
                      n.datum.value.fields[0].fields[1].fields[0].fields[0].fields[0].bytes
                    ],
                    alternative: 0
                  }
                ],
                alternative: 0
              }
            ]
          }
        ]
      }), i) {
        const o = {
          alternative: 0,
          fields: [i, n.datum.value.fields[1].int]
        };
        t.push({
          utxo: r,
          datum: o
        });
      }
    }
  }), t;
}
class Kn {
  constructor(t) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.address = t.treasury.address, this.contractInfo = this.getContributorReferenceInfo();
  }
  async getContributorReferenceInfo() {
    const n = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      {
        contractAddress: this.andamioConfig.contributorReference.address
      }
    );
    return {
      utxos: rn(
        n.utxos
      ),
      data: []
    };
  }
}
function on(e) {
  var t, n, s, r, i, o;
  if (((t = e.graphQLUTxO.datum) == null ? void 0 : t.value.fields[0].fields[1].constructor) == 1)
    return {
      alternative: 0,
      fields: [
        {
          alternative: 0,
          fields: [
            {
              alternative: 0,
              fields: [
                (n = e == null ? void 0 : e.graphQLUTxO.datum) == null ? void 0 : n.value.fields[0].fields[0].fields[0].bytes
              ]
            },
            {
              alternative: 1,
              fields: []
            }
          ]
        },
        (s = e == null ? void 0 : e.graphQLUTxO.datum) == null ? void 0 : s.value.fields[1].int
      ]
    };
  if (((r = e.graphQLUTxO.datum) == null ? void 0 : r.value.fields[0].fields[1].constructor) == 0)
    return {
      alternative: 0,
      fields: [
        {
          alternative: 0,
          fields: [
            {
              alternative: 0,
              fields: [
                (i = e == null ? void 0 : e.graphQLUTxO.datum) == null ? void 0 : i.value.fields[0].fields[0].fields[0].bytes
              ]
            },
            {
              alternative: 0,
              fields: [
                {
                  fields: [
                    {
                      fields: [
                        e.graphQLUTxO.datum.value.fields[0].fields[1].fields[0].fields[0].fields[0].bytes
                      ],
                      alternative: 0
                    }
                  ],
                  alternative: 0
                }
              ]
            }
          ]
        },
        (o = e == null ? void 0 : e.graphQLUTxO.datum) == null ? void 0 : o.value.fields[1].int
      ]
    };
  throw new Error("contributorReferenceUTxO is undefined");
}
function ve(e) {
  let t;
  return e.length > 64 ? t = {
    alternative: 0,
    fields: [
      { alternative: 0, fields: [ge(e)] },
      {
        alternative: 0,
        fields: [
          {
            fields: [
              {
                fields: [Ze(e)],
                alternative: 0
              }
            ],
            alternative: 0
          }
        ]
      }
    ]
  } : t = {
    alternative: 0,
    fields: [
      { alternative: 0, fields: [ge(e)] },
      { alternative: 1, fields: [] }
    ]
  }, t;
}
function an(e, t) {
  if (!t.datum)
    throw new Error("No datum found");
  const s = {
    alternative: 0,
    fields: [
      ve(e),
      t.datum.value.fields[1].int
    ]
  }, r = s.fields[1] + 1;
  return {
    alternative: 0,
    fields: [s.fields[0], r]
  };
}
const cn = {
  data: {
    alternative: 0,
    fields: []
  },
  budget: {
    mem: 47e5,
    steps: 5e9
  }
}, Oe = oe`
  query GetUTxOWithAsset($address: String!, $assetId: Hex!) {
    utxos(
      where: {
        _and: [
          { address: { _eq: $address } }
          { tokens: { asset: { assetId: { _eq: $assetId } } } }
        ]
      }
    ) {
      txHash
      index
      address
      value
      tokens {
        asset {
          policyId
          assetName
          assetId
          fingerprint
        }
        quantity
      }
      datum {
        bytes
        value
      }
    }
  }
`;
function P({
  utxoFromQuery: e
}) {
  var r;
  const n = [{
    unit: "lovelace",
    quantity: e.value
  }];
  return e.tokens && e.tokens.forEach((i) => {
    const o = {
      unit: i.asset.policyId + i.asset.assetName,
      quantity: i.quantity
    };
    n.push(o);
  }), {
    input: {
      outputIndex: e.index,
      txHash: e.txHash
    },
    output: {
      address: e.address,
      amount: n,
      plutusData: (r = e.datum) == null ? void 0 : r.bytes
    }
  };
}
async function qe({
  andamioConfig: e,
  contribToken: t
}) {
  const n = t.slice(0, 56) + "313030" + t.slice(62), s = new k({ andamioConfig: e });
  let r;
  try {
    r = await s.graphQLClient.request(Oe, {
      address: e.contributorReference.address,
      assetId: n
    });
  } catch (i) {
    throw console.error("Error fetching UTxOs:", i), i;
  }
  if (r && r.utxos.length === 1) {
    const i = P({ utxoFromQuery: r.utxos[0] });
    return { graphQLUTxO: r.utxos[0], meshUTxO: i };
  }
  throw new Error("Matching Reference Token not found");
}
async function Zn({
  andamioConfig: e
}) {
  const t = new k({ andamioConfig: e });
  try {
    const n = await t.graphQLClient.request(
      F,
      {
        contractAddress: e.contributorReference.address
      }
    ), s = n.utxos.map(
      (r) => P({ utxoFromQuery: r })
    );
    return { graphQLUTxOs: n.utxos, meshUTxOs: s };
  } catch (n) {
    throw console.error("Error fetching UTxOs:", n), n;
  }
}
function He(e) {
  const t = {
    version: "V2",
    code: e.contributorReference.cbor
  };
  return {
    input: {
      outputIndex: e.contributorReference.referenceTxIx,
      txHash: e.contributorReference.referenceTxHash
    },
    output: {
      address: e.contributorReference.referenceUTxOAddress,
      amount: [
        {
          unit: "lovelace",
          quantity: e.contributorReference.referenceTxLovelace
        }
      ],
      scriptRef: J(t)
    }
  };
}
function un({
  inputUTxO: e,
  refUTxO: t,
  contributorAddress: n
}) {
  const s = ve(n);
  if (s)
    return {
      value: e,
      script: t,
      datum: e,
      redeemer: {
        data: {
          alternative: 1,
          fields: [s]
        }
      }
    };
  throw new Error(
    "Could not construct ContributorReferenceInput for UpdateAddressTx"
  );
}
function dn({
  validatorAddress: e,
  contributorRefInput: t,
  contributorAddress: n
}) {
  var h;
  const s = e, r = ve(n), i = (h = t.graphQLUTxO.datum) == null ? void 0 : h.value.fields[1].int, o = t.graphQLUTxO.tokens[0].asset.assetId, c = t.graphQLUTxO.value;
  return {
    recipient: {
      address: s,
      datum: { value: {
        alternative: 0,
        fields: [r, i]
      }, inline: !0 }
    },
    value: {
      output: {
        address: e,
        amount: [
          {
            unit: "lovelace",
            quantity: c
          },
          {
            unit: o,
            quantity: "1"
          }
        ]
      }
    }
  };
}
function ln({
  address: e,
  contributorAsset: t
}) {
  return { recipient: { address: e }, value: {
    output: {
      address: e,
      amount: [
        {
          unit: "lovelace",
          quantity: "1500000"
        },
        {
          unit: t,
          quantity: "1"
        }
      ]
    }
  } };
}
class es {
  constructor(t, n) {
    x(this, "contribRefRefScriptUtxo");
    x(this, "contributorAddress");
    x(this, "contributorTokenName");
    x(this, "contributorTokenAsset");
    x(this, "queryContribRefTokenUtxo");
    x(this, "contributorReferenceInput");
    x(this, "contributorReferenceDatum");
    x(this, "contributorReferenceOutput");
    x(this, "contribTokenBackToContributor");
    this.wallet = t, this.andamioConfig = n, this.contribRefRefScriptUtxo = He(n), this.runQueries().then(() => this.initialize());
  }
  async runQueries() {
    this.contributorAddress = await this.wallet.getChangeAddress();
    const t = await this.wallet.getPolicyIdAssets(
      this.andamioConfig.tokens.contributorPolicyID
    );
    t && (this.contributorTokenName = t[0].assetName, this.contributorTokenAsset = t[0].unit);
  }
  initialize() {
    this.contributorTokenAsset && this.queryContribRefTokenUtxo && this.contributorAddress && (this.contributorReferenceDatum = on(
      this.queryContribRefTokenUtxo
    ), this.contributorReferenceInput = un({
      inputUTxO: this.queryContribRefTokenUtxo.meshUTxO,
      refUTxO: this.contribRefRefScriptUtxo,
      contributorAddress: this.contributorAddress
    }), this.contributorReferenceOutput = dn({
      validatorAddress: this.andamioConfig.contributorReference.address,
      contributorRefInput: this.queryContribRefTokenUtxo,
      contributorAddress: this.contributorAddress
    }), this.contribTokenBackToContributor = ln({
      address: this.contributorAddress,
      contributorAsset: this.contributorTokenAsset
    }));
  }
  async showContribRefDatum() {
    return await this.runQueries(), this.contributorReferenceDatum;
  }
  async showTxInfo() {
    return await this.runQueries(), await this.initialize(), {
      contractIn: this.contributorReferenceInput,
      contractOut: this.contributorReferenceOutput,
      contribOut: this.contribTokenBackToContributor
    };
  }
  async updateAddress() {
    if (await this.runQueries(), await this.initialize(), console.log("A:", this.contributorReferenceInput), console.log("B:", this.contributorReferenceOutput), console.log("C:", this.contribTokenBackToContributor), this.contributorReferenceInput && this.contributorReferenceOutput && this.contribTokenBackToContributor) {
      const t = new me({ initiator: this.wallet }).redeemValue(this.contributorReferenceInput).sendValue(
        this.contributorReferenceOutput.recipient,
        this.contributorReferenceOutput.value
      ).sendValue(
        this.contribTokenBackToContributor.recipient,
        this.contribTokenBackToContributor.value
      );
      console.log(t);
      const n = await t.build();
      console.log(n);
      const s = await this.wallet.signTx(n, !0), r = await this.wallet.submitTx(s);
      console.log("Success!", r);
    } else
      console.log("Cannot build that Tx, try again.");
  }
}
function hn(e, t) {
  const n = e.escrows.find(
    (s) => s.address === t
  );
  if (n)
    return e.tokens.contractTokenPolicyID + n.contractTokenName;
  throw new Error("Escrow corresponding to input address not found");
}
function fn(e, t, n) {
  const s = [], r = hn(
    e,
    t[0].address
  );
  return t.forEach((i) => {
    var c, d;
    const o = i.tokens.find(
      (l) => l.asset.policyId == n
    );
    if (o && i.datum) {
      const l = o.asset.assetId, h = i.datum, T = {
        contractToken: r,
        projectHashId: h.value.fields[0].bytes,
        expirationTime: h.value.fields[1].int,
        adaAmount: parseInt(i.value),
        projectTokenAmount: parseInt(
          (d = (c = i.tokens.find(
            (E) => E.asset.policyId === e.tokens.projectTokenPolicyID
          )) == null ? void 0 : c.quantity) != null ? d : "0"
        ),
        contributorAsset: l
      };
      s.push(T);
    }
  }), s;
}
class ts {
  constructor(t, n) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.escrowIndex = n, this.address = t.treasury.address, this.contractInfo = this.getEscrowInfo(n);
  }
  async getEscrowInfo(t) {
    const s = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      {
        contractAddress: this.andamioConfig.escrows[t].address
      }
    ), r = fn(
      this.andamioConfig,
      s.utxos,
      this.andamioConfig.tokens.contributorPolicyID
    ), i = [];
    return s.utxos.forEach((o) => {
      var l, h, T;
      const c = {
        alternative: 0,
        fields: [
          (l = o.datum) == null ? void 0 : l.value.fields[0].bytes,
          (h = o.datum) == null ? void 0 : h.value.fields[1].int,
          {
            alternative: 0,
            fields: [
              {
                alternative: 1,
                fields: [
                  (T = o.datum) == null ? void 0 : T.value.fields[2].fields[0].fields[0].bytes
                ]
              },
              { alternative: 1, fields: [] }
            ]
          },
          {
            alternative: 0,
            fields: []
          }
        ]
      }, d = {
        utxo: P({ utxoFromQuery: o }),
        datum: c
      };
      i.push(d);
    }), {
      utxos: i,
      data: r
    };
  }
}
function pn(e, t) {
  const s = {
    alternative: 0,
    fields: [
      {
        alternative: 1,
        fields: [et(
          t.treasury.address
        )]
      },
      { alternative: 1, fields: [] }
    ]
  };
  return {
    alternative: 0,
    fields: [
      e.projectHashId,
      e.expirationTime,
      s,
      { alternative: 0, fields: [] }
    ]
  };
}
const Tn = {
  data: {
    alternative: 1,
    fields: []
  },
  budget: {
    mem: 93e5,
    steps: 5e9
  }
};
async function ns({
  andamioConfig: e,
  escrow: t
}) {
  const n = new k({ andamioConfig: e });
  try {
    const s = await n.graphQLClient.request(
      F,
      {
        contractAddress: t.address
      }
    ), r = s.utxos.map(
      (i) => P({ utxoFromQuery: i })
    );
    return { graphQLUTxOs: s.utxos, meshUTxOs: r };
  } catch (s) {
    throw console.error("Error fetching UTxOs:", s), s;
  }
}
const mn = oe`
  query TransactionWithHash($hash: Hash32Hex!) {
    transactions(where: { hash: { _eq: $hash } }) {
      hash
      includedAt
      metadata {
        key
        value
      }
      inputs {
        address
        sourceTxHash
        sourceTxIndex
        value
        tokens {
          asset {
            policyId
            assetName
            assetId
          }
          quantity
        }
      }
      outputs {
        txHash
        index
        address
        value
        tokens {
          asset {
            policyId
            assetName
            assetId
          }
          quantity
        }
        datum {
          value
        }
      }
    }
  }
`;
function yn({
  andamioConfig: e,
  committedUTxO: t
}) {
  const n = t.output.amount.find(
    (s) => s.unit.includes(e.tokens.contributorPolicyID)
  );
  if (n)
    return n.unit;
  throw new Error("No contributor token found. Check input UTxO.");
}
async function En({
  andamioConfig: e,
  committedUTxO: t
}) {
  const n = new k({ andamioConfig: e }), s = yn({
    andamioConfig: e,
    committedUTxO: t
  }), i = (await n.graphQLClient.request(
    mn,
    {
      hash: t.input.txHash
    }
  )).transactions[0];
  if (i) {
    const o = i.inputs.find(
      (c) => c.tokens.some(
        (d) => s.includes(d.asset.policyId)
      )
    );
    if (o)
      return o.address;
  }
  throw new Error("Error finding contributor address for the committed UTxO.");
}
function xn({
  andamioConfig: e,
  committedUTxO: t
}) {
  const s = e.escrows.find(
    (r) => r.address === t.output.address
  );
  if (s)
    return s;
  throw new Error("No matching escrow found. Check input UTxO.");
}
function be({
  selectedProject: e,
  andamioConfig: t
}) {
  const n = t.escrows.find(
    (s) => e.contractToken.includes(s.contractTokenName)
  );
  if (!n)
    throw new Error("Escrow corresponding to Selected Project not found.");
  return n;
}
function vn(e, t) {
  const n = t.meshUTxO.output.amount.find(
    (r) => r.unit !== "lovelace"
  ), s = e.escrows.find(
    (r) => n == null ? void 0 : n.unit.includes(r.contractTokenName)
  );
  if (!s)
    throw new Error("Escrow match is not found.");
  return s;
}
function ss({
  andamioConfig: e,
  committedUTxO: t
}) {
  const n = xn({
    andamioConfig: e,
    committedUTxO: t
  }), s = {
    version: "V2",
    code: n.cbor
  };
  return {
    input: {
      outputIndex: n.referenceTxIx,
      txHash: n.referenceTxHash
    },
    output: {
      address: n.referenceUTxOAddress,
      amount: [
        {
          unit: "lovelace",
          quantity: n.referenceTxLovelace
        }
      ],
      scriptRef: J(s)
    }
  };
}
function rs(e) {
  const t = {
    version: "V2",
    code: e.cbor
  };
  return {
    input: {
      outputIndex: e.referenceTxIx,
      txHash: e.referenceTxHash
    },
    output: {
      address: e.referenceUTxOAddress,
      amount: [
        {
          unit: "lovelace",
          quantity: e.referenceTxLovelace
        }
      ],
      scriptRef: J(t)
    }
  };
}
function On(e, t) {
  const n = e.escrows.find(
    (s) => t.includes(s.contractTokenName)
  );
  if (n) {
    const s = {
      version: "V2",
      code: n.cbor
    };
    return {
      input: {
        outputIndex: n.referenceTxIx,
        txHash: n.referenceTxHash
      },
      output: {
        address: n.referenceUTxOAddress,
        amount: [
          {
            unit: "lovelace",
            quantity: n.referenceTxLovelace
          }
        ],
        scriptRef: J(s)
      }
    };
  }
  throw new Error("Escrow corresponding to input Contract Token not found");
}
async function bn(e, t) {
  if (!t.contributorAsset)
    throw new Error(
      "Project must have a contributor to find the commitment UTxO"
    );
  const n = new k({ andamioConfig: e });
  let s;
  try {
    s = await n.graphQLClient.request(Oe, {
      address: be({ selectedProject: t, andamioConfig: e }).address,
      assetId: t.contributorAsset
    });
  } catch (r) {
    throw console.error("Error fetching UTxOs:", r), r;
  }
  if (s && s.utxos.length === 1) {
    const r = P({ utxoFromQuery: s.utxos[0] });
    return { graphQLUTxO: s.utxos[0], meshUTxO: r };
  }
  throw new Error("Commitment not found");
}
function gn(e, t) {
  return {
    value: e.meshUTxO,
    script: t,
    datum: e.meshUTxO,
    redeemer: Tn
  };
}
function An(e, t) {
  return {
    value: e.meshUTxO,
    script: t,
    datum: e.meshUTxO,
    redeemer: cn
  };
}
function _n(e, t) {
  if (!e.contributorAsset)
    throw new Error("Project must have a contributor");
  return {
    recipient: {
      address: t
    },
    value: {
      output: {
        address: t,
        amount: [
          {
            unit: "lovelace",
            quantity: "2000000"
          },
          {
            unit: e.contributorAsset,
            quantity: "1"
          }
        ]
      }
    }
  };
}
function Cn(e, t) {
  return {
    recipient: { address: e },
    value: {
      output: {
        address: e,
        amount: [
          {
            unit: "lovelace",
            quantity: "2000000"
          },
          {
            unit: t,
            quantity: "1"
          }
        ]
      }
    }
  };
}
function In(e, t, n) {
  var i, o;
  const s = (i = n.output.amount.find(
    (c) => c.unit === "lovelace"
  )) == null ? void 0 : i.quantity, r = (o = n.output.amount.find(
    (c) => c.unit === e.tokens.projectAsset
  )) == null ? void 0 : o.quantity;
  if (!s || !r)
    throw new Error("Rewards not found");
  return {
    recipient: {
      address: t
    },
    value: {
      output: {
        address: t,
        amount: [
          {
            unit: "lovelace",
            quantity: (parseInt(s) - 2e6).toString()
          },
          {
            unit: e.tokens.projectAsset,
            quantity: r
          }
        ]
      }
    }
  };
}
function Nn(e, t, n) {
  return {
    recipient: {
      address: e.contributorReference.address,
      datum: {
        value: an(
          n,
          t.graphQLUTxO
        ),
        inline: !0
      }
    },
    value: {
      output: t.meshUTxO.output
    }
  };
}
function Qe(e) {
  var r, i;
  const t = [], n = [];
  return (r = e.datum) == null || r.value.fields[0].fields[0].list.forEach(
    (o) => {
      const c = {
        alternative: 0,
        fields: [
          o.fields[0].bytes,
          o.fields[1].int,
          o.fields[2].int,
          o.fields[3].int
        ]
      };
      t.push(c);
    }
  ), (i = e.datum) == null || i.value.fields[0].fields[1].list.forEach(
    (o) => n.push(o.bytes)
  ), {
    alternative: 1,
    fields: [
      {
        alternative: 0,
        fields: [t, n]
      }
    ]
  };
}
function wn({
  selectedProject: e,
  contractTokenUtxo: t
}) {
  const n = Qe(t), s = {
    alternative: 0,
    fields: [
      e.projectHashId,
      e.expirationTime,
      e.adaAmount,
      e.projectTokenAmount
    ]
  }, r = n.fields[0].fields[0].filter(
    (c) => JSON.stringify(c) !== JSON.stringify(s)
  ), i = n.fields[0].fields[1];
  return {
    alternative: 1,
    fields: [
      {
        alternative: 0,
        fields: [r, i]
      }
    ]
  };
}
const $e = {
  alternative: 0,
  fields: []
};
class is {
  constructor(t) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.address = t.treasury.address, this.contractInfo = this.getTreasuryInfo();
  }
  async getTreasuryInfo() {
    const n = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      { contractAddress: this.andamioConfig.treasury.address }
    ), s = [], r = [];
    n.utxos.forEach((d) => {
      const l = P({ utxoFromQuery: d });
      if (l.output.plutusData == "d87980") {
        const h = {
          utxo: l,
          datum: $e
        };
        r.push(h);
      } else {
        const h = {
          utxo: l,
          datum: Qe(d),
          contractToken: l.output.amount[1].unit
        };
        s.push(h);
      }
    });
    let i = 0, o = 0;
    r.forEach((d) => {
      i += parseInt(d.utxo.output.amount[0].quantity), o += parseInt(d.utxo.output.amount[1].quantity);
    });
    const c = [];
    return s.forEach((d) => {
      d.datum.fields[0].fields[0].forEach((h) => {
        if (d.contractToken) {
          const T = {
            contractToken: d.contractToken,
            projectHashId: h.fields[0],
            expirationTime: h.fields[1],
            adaAmount: h.fields[2],
            projectTokenAmount: h.fields[3]
          };
          c.push(T);
        }
      });
    }), {
      utxos: s,
      data: c,
      numFundUTxOs: r.length,
      numContractUTxOs: s.length,
      fundUTxOs: r,
      contractUTxOs: s,
      totalLovelace: i,
      totalTokens: o
    };
  }
}
function kn({ selectedProject: e }) {
  return {
    data: {
      alternative: 1,
      fields: [
        {
          alternative: 0,
          fields: [
            e.adaAmount,
            e.projectTokenAmount
          ]
        }
      ]
    }
  };
}
function Rn({
  selectedProject: e
}) {
  const { projectHashId: t, expirationTime: n, adaAmount: s, projectTokenAmount: r } = e;
  return {
    data: {
      alternative: 0,
      fields: [
        {
          alternative: 0,
          fields: [
            t,
            n,
            s,
            r
          ]
        }
      ]
    }
  };
}
async function os({
  andamioConfig: e
}) {
  const t = new k({ andamioConfig: e });
  try {
    const n = await t.graphQLClient.request(
      F,
      {
        contractAddress: e.treasury.address
      }
    ), s = n.utxos.map(
      (r) => P({ utxoFromQuery: r })
    );
    return { graphQLUTxOs: n.utxos, meshUTxOs: s };
  } catch (n) {
    throw console.error("Error fetching UTxOs:", n), n;
  }
}
function as(e) {
  var c;
  const t = e.graphQLUTxOs, n = [], s = { fields: [], constructor: 0 };
  for (const d of t)
    if (JSON.stringify((c = d.datum) == null ? void 0 : c.value) === JSON.stringify(s)) {
      const l = t.findIndex((h) => h === d);
      n.push(l);
    }
  if (n.length === 0)
    throw new Error("No funds UTxO found!");
  const r = e.meshUTxOs, i = n.map((d) => t[d]), o = n.map((d) => r[d]);
  return { graphQLUTxOs: i, meshUTxOs: o };
}
function Sn({
  escrow: e,
  currentTreasuryUTxOs: t
}) {
  const n = t.meshUTxOs.findIndex(
    (s) => s.output.amount.some(
      (r) => r.unit.includes(e.contractTokenName)
    )
  );
  if (n != -1)
    return {
      graphQLUTxO: t.graphQLUTxOs[n],
      meshUTxO: t.meshUTxOs[n]
    };
  throw new Error("No contract UTxO found. Check input escrow.");
}
function Dn(e) {
  const t = {
    version: "V2",
    code: e.treasury.cbor
  };
  return {
    input: {
      outputIndex: e.treasury.referenceTxIx,
      txHash: e.treasury.referenceTxHash
    },
    output: {
      address: e.treasury.referenceUTxOAddress,
      amount: [
        {
          unit: "lovelace",
          quantity: e.treasury.referenceTxLovelace
        }
      ],
      scriptRef: J(t)
    }
  };
}
function cs(e, t, n) {
  const s = be({
    selectedProject: e,
    andamioConfig: t
  });
  return Sn({
    escrow: s,
    currentTreasuryUTxOs: n
  });
}
function us({
  currentTreasuryUTxOs: e
}) {
  const t = e, n = t.graphQLUTxOs, s = [], r = {
    fields: [],
    constructor: 0
  };
  for (const d of n)
    if (d.datum && d.datum.value === r) {
      const l = n.findIndex((h) => h === d);
      s.push(l);
    }
  const i = t.meshUTxOs, o = n.filter(
    (d, l) => !s.includes(l)
  ), c = i.filter(
    (d, l) => !s.includes(l)
  );
  return {
    graphQLUTxOs: o,
    meshUTxOs: c
  };
}
const Un = oe`
  query GetUTxOWithAsset($datumHash: Hex!, $address: String!) {
    utxos(
      where: {
        _and: [
          { address: { _eq: $address } }
          { datum: { hash: { _eq: $datumHash } } }
        ]
      }
    ) {
      txHash
      index
      address
      value
      tokens {
        asset {
          policyId
          assetName
          assetId
          fingerprint
        }
        quantity
      }
      datum {
        hash
        bytes
        value
      }
    }
  }
`;
async function Ln(e) {
  const t = new k({ andamioConfig: e });
  try {
    const n = await t.graphQLClient.request(
      Un,
      {
        datumHash: "923918e403bf43c34b4ef6b48eb2ee04babed17320d8d1b9ff9ad086e86f44ec",
        address: e.treasury.address
      }
    ), s = n.utxos.map(
      (r) => P({ utxoFromQuery: r })
    );
    return { graphQLUTxOs: n.utxos, meshUTxOs: s };
  } catch (n) {
    throw console.error("Error fetching UTxOs:", n), n;
  }
}
async function Ge(e, t) {
  const n = new k({ andamioConfig: e });
  let s;
  try {
    s = await n.graphQLClient.request(Oe, {
      address: e.treasury.address,
      assetId: t
    });
  } catch (r) {
    throw console.error("Error fetching UTxOs:", r), r;
  }
  if (s && s.utxos.length === 1) {
    const r = P({ utxoFromQuery: s.utxos[0] });
    return { graphQLUTxO: s.utxos[0], meshUTxO: r };
  }
  throw new Error("Contract Token UTxO not found");
}
function Fn(e, t, n) {
  const s = Rn({
    selectedProject: e
  });
  return {
    value: t,
    script: n,
    datum: t,
    redeemer: s
  };
}
function Pn(e, t, n) {
  const s = kn({ selectedProject: e });
  return {
    value: t,
    script: n,
    datum: t,
    redeemer: s
  };
}
function Bn(e, t, n, s) {
  const r = [
    {
      unit: "lovelace",
      quantity: e.adaAmount.toString()
    },
    {
      unit: t.tokens.projectAsset,
      quantity: e.projectTokenAmount.toString()
    },
    {
      unit: s.unit,
      quantity: "1"
    }
  ], i = {
    output: {
      address: n.address,
      amount: r
    }
  }, o = pn(
    e,
    t
  );
  return {
    recipient: {
      address: n.address,
      datum: {
        value: o,
        inline: !0
      }
    },
    value: i
  };
}
function Mn(e, t, n) {
  const s = n.meshUTxO.output.amount, r = s.find((l) => l.unit === "lovelace"), i = s.find((l) => l.unit !== "lovelace");
  if (!r || !i)
    throw new Error(
      "Assets not found. Check if it is the right contract token utxo."
    );
  const o = [
    {
      unit: "lovelace",
      quantity: r.quantity.toString()
    },
    {
      unit: i.unit.toString(),
      quantity: i.quantity.toString()
    }
  ], c = {
    output: {
      address: t.treasury.address,
      amount: o
    }
  }, d = wn({
    selectedProject: e,
    contractTokenUtxo: n.graphQLUTxO
  });
  return {
    recipient: {
      address: t.treasury.address,
      datum: {
        value: d,
        inline: !0
      }
    },
    value: c
  };
}
function jn(e, t, n) {
  const s = n.output.amount, r = s.filter(
    (b) => b.unit === "lovelace"
  ), o = parseInt(r[0].quantity) - e.adaAmount, c = s.filter(
    (b) => b.unit === t.tokens.projectAsset
  ), l = parseInt(
    c[0].quantity
  ) - e.projectTokenAmount, h = [
    {
      unit: "lovelace",
      quantity: o.toString()
    },
    {
      unit: t.tokens.projectAsset,
      quantity: l.toString()
    }
  ], T = {
    output: {
      address: t.treasury.address,
      amount: h
    }
  }, E = $e;
  return {
    recipient: {
      address: t.treasury.address,
      datum: {
        value: E,
        inline: !0
      }
    },
    value: T
  };
}
class ds {
  constructor(t, n, s) {
    x(this, "initialized");
    x(this, "referenceScriptUTxOs");
    x(this, "inputs");
    x(this, "outputs");
    x(this, "connectedContribToken");
    x(this, "contribRefTokenUTxO");
    x(this, "treasuryContractTokenUTxO");
    x(this, "treasuryFundUTxOs");
    x(this, "escrow");
    this.wallet = t, this.selectedProject = n, this.andamioConfig = s, this.initialized = !1, this.runQueries().then(() => this.initialize());
  }
  async runQueries() {
    this.connectedContribToken = (await this.wallet.getAssets()).find(
      (t) => t.policyId === this.andamioConfig.tokens.contributorPolicyID
    ), this.contribRefTokenUTxO = await qe({
      andamioConfig: this.andamioConfig,
      contribToken: this.connectedContribToken.unit
    }), this.treasuryContractTokenUTxO = await Ge(
      this.andamioConfig,
      this.selectedProject.contractToken
    ), this.treasuryFundUTxOs = await Ln(this.andamioConfig);
  }
  initialize() {
    this.escrow = vn(
      this.andamioConfig,
      this.treasuryContractTokenUTxO
    ), this.referenceScriptUTxOs = {
      treasuryRefScriptUTxO: Dn(this.andamioConfig)
    }, this.inputs = {
      fundsFromTreasury: Pn(
        this.selectedProject,
        this.treasuryFundUTxOs.meshUTxOs[0],
        this.referenceScriptUTxOs.treasuryRefScriptUTxO
      ),
      contractTokenFromTreasury: Fn(
        this.selectedProject,
        this.treasuryContractTokenUTxO.meshUTxO,
        this.referenceScriptUTxOs.treasuryRefScriptUTxO
      )
    }, this.outputs = {
      commitmentToEscrow: Bn(
        this.selectedProject,
        this.andamioConfig,
        this.escrow,
        this.connectedContribToken
      ),
      contractTokenBackToTreasury: Mn(
        this.selectedProject,
        this.andamioConfig,
        this.treasuryContractTokenUTxO
      ),
      residueFundsToTreasury: jn(
        this.selectedProject,
        this.andamioConfig,
        this.treasuryFundUTxOs.meshUTxOs[0]
      )
    }, this.initialized = !0;
  }
  async runTx() {
    if (this.initialized) {
      const t = new me({ initiator: this.wallet }).redeemValue(this.inputs.fundsFromTreasury).redeemValue(this.inputs.contractTokenFromTreasury).sendValue(
        this.outputs.residueFundsToTreasury.recipient,
        this.outputs.residueFundsToTreasury.value
      ).sendValue(
        this.outputs.contractTokenBackToTreasury.recipient,
        this.outputs.contractTokenBackToTreasury.value
      ).sendValue(
        this.outputs.commitmentToEscrow.recipient,
        this.outputs.commitmentToEscrow.value
      ).setTxRefInputs([this.contribRefTokenUTxO.meshUTxO]).setChangeAddress(await this.wallet.getChangeAddress());
      console.log("Transaction successfully built:", t);
      const n = await t.build();
      return console.log(n), Re(this.wallet, n);
    } else
      throw new Error(
        "Instance variables are not yet initialized. Please try again after some time."
      );
  }
  txDetails() {
    if (this.initialized)
      return {
        fundsFromTreasury: { redeemValue: this.inputs.fundsFromTreasury },
        contractTokenFromTreasury: {
          redeemValue: this.inputs.contractTokenFromTreasury
        },
        residueFundsToTreasury: {
          sendValue: this.outputs.residueFundsToTreasury
        },
        contractTokenBackToTreasury: {
          sendValue: this.outputs.contractTokenBackToTreasury
        },
        commitmentToEscrow: { sendValue: this.outputs.commitmentToEscrow },
        connectedRefTokenUTxO: {
          setTxRefInputs: [this.contribRefTokenUTxO.meshUTxO]
        }
      };
    throw new Error(
      "Instance variables are not yet initialized. Please try again after some time."
    );
  }
}
class ls {
  constructor(t, n, s) {
    x(this, "initialized");
    x(this, "referenceScriptUTxOs");
    x(this, "inputs");
    x(this, "outputs");
    x(this, "ESCROW");
    x(this, "changeAddress");
    x(this, "connectedDeciderToken");
    x(this, "commitmentUTxO");
    x(this, "contribRefTokenUTxO");
    x(this, "contribAddress");
    x(this, "treasuryContractTokenUTxO");
    this.wallet = t, this.selectedProject = n, this.andamioConfig = s, this.ESCROW = be({
      selectedProject: this.selectedProject,
      andamioConfig: this.andamioConfig
    }), this.initialized = !1, this.runQueries().then(() => this.initialize());
  }
  async runQueries() {
    if (this.changeAddress = await this.wallet.getChangeAddress(), this.connectedDeciderToken = (await this.wallet.getAssets()).find(
      (t) => t.policyId === this.ESCROW.deciderPolicyID
    ), !this.connectedDeciderToken)
      throw new Error("Decider token for this project not found");
    if (this.commitmentUTxO = await bn(
      this.andamioConfig,
      this.selectedProject
    ), !this.selectedProject.contributorAsset)
      throw new Error("Project must have a contributor");
    this.contribRefTokenUTxO = await qe({
      andamioConfig: this.andamioConfig,
      contribToken: this.selectedProject.contributorAsset
    }), this.contribAddress = await En({
      andamioConfig: this.andamioConfig,
      committedUTxO: this.commitmentUTxO.meshUTxO
    }), this.treasuryContractTokenUTxO = await Ge(
      this.andamioConfig,
      this.selectedProject.contractToken
    );
  }
  initialize() {
    this.referenceScriptUTxOs = {
      escrowRefScriptUTxO: On(
        this.andamioConfig,
        this.selectedProject.contractToken
      ),
      contribRefScriptUTxO: He(this.andamioConfig)
    }, this.inputs = {
      commitmentFromEscrow: gn(
        this.commitmentUTxO,
        this.referenceScriptUTxOs.escrowRefScriptUTxO
      ),
      contribRefTokenFromContribRef: An(
        this.contribRefTokenUTxO,
        this.referenceScriptUTxOs.contribRefScriptUTxO
      )
    }, this.outputs = {
      contribTokenToContrib: _n(
        this.selectedProject,
        this.contribAddress
      ),
      deciderToConnectedWallet: Cn(
        this.changeAddress,
        this.connectedDeciderToken.unit
      ),
      updatedContribRefTokenToContribRef: Nn(
        this.andamioConfig,
        this.contribRefTokenUTxO,
        this.contribAddress
      ),
      rewardToContrib: In(
        this.andamioConfig,
        this.contribAddress,
        this.commitmentUTxO.meshUTxO
      )
    }, this.initialized = !0;
  }
  async runTx() {
    if (this.initialized) {
      const t = new me({ initiator: this.wallet }).redeemValue(this.inputs.commitmentFromEscrow).redeemValue(this.inputs.contribRefTokenFromContribRef).sendValue(
        this.outputs.updatedContribRefTokenToContribRef.recipient,
        this.outputs.updatedContribRefTokenToContribRef.value
      ).sendValue(
        this.outputs.rewardToContrib.recipient,
        this.outputs.rewardToContrib.value
      ).sendValue(
        this.outputs.contribTokenToContrib.recipient,
        this.outputs.contribTokenToContrib.value
      ).sendValue(
        this.outputs.deciderToConnectedWallet.recipient,
        this.outputs.deciderToConnectedWallet.value
      ).setTxRefInputs([this.treasuryContractTokenUTxO.meshUTxO]).setChangeAddress(this.changeAddress);
      console.log("Transaction successfully built:", t);
      const n = await t.build();
      return console.log(n), Re(this.wallet, n);
    } else
      throw new Error(
        "Instance variables are not yet initialized. Please try again after some time."
      );
  }
  txDetails() {
    if (this.initialized)
      return {
        commitmentFromEscrow: {
          redeemValue: this.inputs.commitmentFromEscrow
        },
        contribRefTokenFromContribRef: {
          redeemValue: this.inputs.contribRefTokenFromContribRef
        },
        updatedContribRefTokenToContribRef: {
          sendValue: this.outputs.updatedContribRefTokenToContribRef
        },
        rewardToContrib: {
          sendValue: this.outputs.rewardToContrib
        },
        contribTokenToContrib: {
          sendValue: this.outputs.contribTokenToContrib
        },
        deciderToConnectedWallet: {
          sendValue: this.outputs.deciderToConnectedWallet
        },
        treasuryContractTokenUTxO: {
          setTxRefInputs: [this.treasuryContractTokenUTxO.meshUTxO]
        }
      };
    throw new Error(
      "Instance variables are not yet initialized. Please try again after some time."
    );
  }
}
function Vn(e) {
  const t = [];
  return e.forEach((n) => {
    if (n.datum) {
      const s = [{ unit: "lovelace", quantity: n.value }];
      n.tokens.forEach((E) => {
        s.push({ unit: E.asset.assetId, quantity: E.quantity });
      });
      const r = {
        input: {
          txHash: n.txHash,
          outputIndex: n.index
        },
        output: {
          address: n.address,
          amount: s,
          plutusData: n.datum.bytes
        }
      }, i = n.datum.value.fields[0].bytes;
      let o;
      n.datum.value.fields[1].constructor == 0 ? o = {
        alternative: 0,
        fields: [n.datum.value.fields[1].fields[0].bytes]
      } : o = {
        alternative: 1,
        fields: []
      };
      const c = n.datum.value.fields[2].fields[0].fields[0].bytes, d = n.datum.value.fields[2].fields[1].bytes, l = {
        alternative: 0,
        fields: [
          {
            alternative: 0,
            fields: [c]
          },
          d
        ]
      }, h = n.datum.value.fields[3].bytes, T = {
        alternative: 0,
        fields: [
          i,
          o,
          l,
          h
        ]
      };
      t.push({
        utxo: r,
        datum: T
      });
    }
  }), t;
}
class hs {
  constructor(t) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.address = t.assignment.address;
  }
  async getAssignmentInfo() {
    const n = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      {
        contractAddress: this.andamioConfig.assignment.address
      }
    );
    return {
      utxos: Vn(n.utxos),
      data: []
    };
  }
}
function ze(e) {
  let t;
  return e.fields[1].constructor == 0 ? t = {
    alternative: 0,
    fields: [
      {
        alternative: 0,
        fields: [e.fields[0].fields[0].bytes]
      },
      {
        alternative: 0,
        fields: [
          {
            fields: [
              {
                fields: [
                  e.fields[1].fields[0].fields[0].fields[0].bytes
                ],
                alternative: 0
              }
            ],
            alternative: 0
          }
        ]
      }
    ]
  } : t = {
    alternative: 0,
    fields: [
      {
        alternative: 0,
        fields: [e.fields[0].fields[0].bytes]
      },
      { alternative: 1, fields: [] }
    ]
  }, console.log("Here is your Plutus Address:", t), t;
}
function qn(e) {
  const t = [];
  return e.forEach((n) => {
    if (n.datum) {
      const s = [{ unit: "lovelace", quantity: n.value }];
      n.tokens.forEach((d) => {
        s.push({ unit: d.asset.assetId, quantity: d.quantity });
      });
      const r = {
        input: {
          txHash: n.txHash,
          outputIndex: n.index
        },
        output: {
          address: n.address,
          amount: s,
          plutusData: n.datum.bytes
        }
      }, i = n.datum.value.fields[0].bytes, o = [];
      n.datum.value.fields[1].list.forEach((d) => {
        const l = d.fields[0].bytes, h = d.fields[1].bytes;
        o.push({ alternative: 0, fields: [l, h] });
      });
      let c;
      if (n.datum.value.fields[2].constructor == 0) {
        const d = n.datum.value.fields[2].fields[0].fields[0].bytes, l = ze(
          n.datum.value.fields[2].fields[0].fields[1].list[0]
        ), h = [];
        n.datum.value.fields[2].fields[0].fields[2].list.forEach(
          (b) => {
            h.push(b.bytes);
          }
        );
        const T = [];
        n.datum.value.fields[2].fields[0].fields[3].list.forEach(
          (b) => {
            T.push(b.bytes);
          }
        ), c = {
          alternative: 0,
          fields: [{
            alternative: 0,
            fields: [
              d,
              [l],
              h,
              T
            ]
          }]
        };
      } else
        c = {
          alternative: 1,
          fields: []
        };
      if (c) {
        const d = {
          alternative: 0,
          fields: [i, o, c]
        };
        t.push({
          utxo: r,
          datum: d
        });
      }
    }
  }), t;
}
class fs {
  constructor(t) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.address = this.andamioConfig.courseReference.address;
  }
  async getCourseReferenceInfo() {
    const n = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      {
        contractAddress: this.andamioConfig.courseReference.address
      }
    );
    return {
      utxos: qn(n.utxos),
      data: []
    };
  }
}
function Hn(e) {
  const t = [];
  return e.forEach((n) => {
    if (n.datum) {
      const s = [{ unit: "lovelace", quantity: n.value }];
      n.tokens.forEach((h) => {
        s.push({ unit: h.asset.assetId, quantity: h.quantity });
      });
      const r = {
        input: {
          txHash: n.txHash,
          outputIndex: n.index
        },
        output: {
          address: n.address,
          amount: s,
          plutusData: n.datum.bytes
        }
      }, i = ze(
        n.datum.value.fields[0]
      ), o = [];
      n.datum.value.fields[1].list.length > 0 && n.datum.value.fields[1].list.forEach((h) => {
        o.push(h.bytes);
      });
      const c = n.datum.value.fields[2].bytes, d = n.datum.value.fields[3].bytes, l = [];
      if (n.datum.value.fields[4].list.length > 0 && n.datum.value.fields[4].list.forEach((h) => {
        l.push(h.bytes);
      }), i) {
        const h = {
          alternative: 0,
          fields: [
            i,
            o,
            c,
            d,
            l
          ]
        };
        t.push({
          utxo: r,
          datum: h
        });
      }
    }
  }), t;
}
class ps {
  constructor(t) {
    x(this, "address");
    x(this, "contractInfo");
    this.andamioConfig = t, this.address = this.andamioConfig.learnerReference.address;
  }
  async getLearnerReferenceInfo() {
    const n = await new k({ andamioConfig: this.andamioConfig }).graphQLClient.request(
      F,
      {
        contractAddress: this.address
      }
    );
    return {
      utxos: Hn(n.utxos),
      data: []
    };
  }
}
export {
  cn as AddCompletedTaskRedeemer,
  hs as AssignmentData,
  ds as CommitTx,
  Kn as ContributorReferenceData,
  on as ContributorReferenceDatumByTokenName,
  fs as CourseReferenceData,
  Zn as CurrentContributorReferenceUTxOs,
  ns as CurrentEscrowUTxOs,
  Tn as DistributeRedeemer,
  ls as DistributeTx,
  ts as EscrowData,
  k as GraphQL,
  ps as LearnerReferenceData,
  Jn as SetExpirationTime,
  Re as SignAndSubmit,
  is as TreasuryData,
  es as UpdateAddressTx,
  an as UpdatedContributorReferenceDatum,
  pn as commitmentDatum,
  Qe as contractTokenDatumByGraphqlUTxO,
  Rn as contractTokenUTxOsRedeemer,
  He as contribRefScriptUTxO,
  qe as contribRefTokenUTxOByContribToken,
  us as currentContractTokenUTxOs,
  as as currentFundUTxOs,
  os as currentTreasuryUTxOs,
  ss as escrowRefScriptUTxOByCommittedUTxO,
  On as escrowRefScriptUTxOByContractToken,
  rs as escrowRefScriptUTxOByEscrow,
  $e as fundUTxOsDatum,
  kn as fundUTxOsRedeemer,
  Xn as getAssetListFromGraphQLUTxO,
  Gn as getConnectedToken,
  rn as getContributorReferenceUTxOs,
  Wn as getCurrentEscrow,
  nt as hexToString,
  Yn as posixToDateString,
  bn as queryCommitmentUTxOBySelectedProject,
  Ge as queryContractTokenUTxOByContractToken,
  Ln as queryFundUTxOsByDatum,
  zn as stringToHex,
  Sn as treasuryContractTokenUTxOByEscrow,
  cs as treasuryContractTokenUTxOBySelectedProject,
  Dn as treasuryRefScriptUTxO,
  wn as updatedContractTokenDatum
};
