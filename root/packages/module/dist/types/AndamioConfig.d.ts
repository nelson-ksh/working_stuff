import { PlutusScript } from '@meshsdk/core';
export declare type AndamioConfig = {
    title: string;
    baseAddress: string;
    enterpriseAddress: string;
    rewardAddress: string;
    metadataKey: string;
    network: '0' | '1';
    tokens: {
        contributorPolicyID: string;
        adminAsset: string;
        adminPolicyID: string;
        adminTokenName: string;
        projectAsset: string;
        projectTokenPolicyID: string;
        projectTokenName: string;
        contractTokenPolicyID: string;
    };
    treasury: {
        address: string;
        cbor: string;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
    escrows: EscrowConfig[];
    contributorReference: {
        address: string;
        cbor: string;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
    contributorMinter: {
        plutusScript: PlutusScript;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
    courseReference: {
        address: string;
        cbor: string;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
    learnerReference: {
        address: string;
        cbor: string;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
    assignment: {
        address: string;
        cbor: string;
        referenceUTxOAddress: string;
        referenceTxHash: string;
        referenceTxIx: number;
        referenceTxLovelace: string;
    };
};
export declare type EscrowConfig = {
    name: string;
    address: string;
    cbor: string;
    contractTokenName: string;
    deciderPolicyID: string;
    referenceUTxOAddress: string;
    referenceTxHash: string;
    referenceTxIx: number;
    referenceTxLovelace: string;
};
//# sourceMappingURL=AndamioConfig.d.ts.map