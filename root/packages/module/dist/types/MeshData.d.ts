import { Action, Data, UTxO } from '@meshsdk/core';
export declare type PlutusAddress = (Data & {
    alternative: 0;
    fields: [
        {
            alternative: 0;
            fields: [string];
        },
        {
            alternative: 1;
            fields: [];
        }
    ];
}) | (Data & {
    alternative: 0;
    fields: [
        {
            alternative: 0;
            fields: [string];
        },
        {
            alternative: 0;
            fields: [
                {
                    fields: [
                        {
                            fields: [string];
                            alternative: 0;
                        }
                    ];
                    alternative: 0;
                }
            ];
        }
    ];
});
export interface DatumProp {
    value: Data;
    inline: boolean;
}
export interface RecipientProp {
    address: string;
    datum?: DatumProp;
}
export interface RedeemValue {
    value: UTxO;
    script: UTxO;
    datum: UTxO;
    redeemer: Partial<Action>;
}
export interface SendValue {
    recipient: RecipientProp;
    value: Partial<UTxO>;
}
export type Nothing = Data & {
    alternative: 1;
    fields: [];
};
//# sourceMappingURL=MeshData.d.ts.map