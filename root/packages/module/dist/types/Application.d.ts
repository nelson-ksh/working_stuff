import { UTxO } from '@meshsdk/core';
import { GraphQLUTxO } from './CardanoGraphQL';
import { RedeemValue, SendValue } from './MeshData';
export interface ReferenceScriptUTxOs {
    readonly [refUTxOName: string]: UTxO;
}
export interface TxInputs {
    readonly [inputName: string]: RedeemValue;
}
export interface TxOutputs {
    readonly [outputName: string]: SendValue;
}
export interface ContractTransaction {
    referenceScriptUTxOs: ReferenceScriptUTxOs;
    inputs: TxInputs;
    outputs: TxOutputs;
    runQueries(): void;
    initialize(): void;
    runTx(): Promise<string>;
}
export interface ContractUTxO<T> {
    utxo: UTxO;
    datum: T;
}
/**
 * ContractInfo can always fetch a list of utxos at the Contract Address.
 * Can also provide arbitrary data, based on needs of contract endpoint.
 * Use this to explore different data fetching functions in a Contract interface
 */
export interface ContractInfo<ContractUTxO, T> {
    utxos: ContractUTxO[];
    data: T[];
}
export interface ContractData<ContractInfo> {
    address: string;
    contractInfo: Promise<ContractInfo>;
}
export interface ResponseUTxOsList {
    graphQLUTxOs: GraphQLUTxO[];
    meshUTxOs: UTxO[];
}
export interface ResponseUTxO {
    graphQLUTxO: GraphQLUTxO;
    meshUTxO: UTxO;
}
//# sourceMappingURL=Application.d.ts.map