import { GraphQLClient } from 'graphql-request';
import { AndamioConfig } from 'src/types';
export declare class GraphQL {
    graphQLClient: GraphQLClient;
    andamioConfig: AndamioConfig;
    graphQlUrl: string;
    constructor({ andamioConfig }: {
        andamioConfig: AndamioConfig;
    });
}
/**
 * DEPRECATED: Todo: In process of moving to /src/contract/contributor
 * Get the history of Commit and Distribute transactions for a Contributor.
 * ContributorTokenSummary includes a list of transactions and a simple list of completed projects
 * The list of projects can be made more robust from this proof of concept
 * @param {string} contributorAssetId
 * @returns Promise<ContributorTokenSummary>
 */
//# sourceMappingURL=index.d.ts.map