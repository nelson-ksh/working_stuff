import { AssetExtended } from '@meshsdk/core';
import { GraphQLUTxO } from 'src/types';
/**
 * From a GraphQL query, get a list of assets in a UTxO, formatted for Mesh
 * @param graphqlUTxO
 * @returns {AssetExtended[]} A list of assets in the UTxO
 */
export declare function getAssetListFromGraphQLUTxO(graphqlUTxO: GraphQLUTxO): AssetExtended[];
//# sourceMappingURL=getAssetListFromGraphQLUTxO.d.ts.map