/**
 *
 * @param {string} str a readable string
 * @returns hex string
 */
export declare function stringToHex(str: string): string;
/**
 * @param {string} input a hex string
 * @returns a readable string
 */
export declare function hexToString(input: string): string;
/**
 *
 * @param posix a posix timestamp
 * @returns YYYY-MM-DD date string
 */
export declare function posixToDateString(posix: number): string;
//# sourceMappingURL=converters.d.ts.map