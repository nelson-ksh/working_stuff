import { EscrowConfig } from 'src/types';
export declare function getCurrentEscrow(contractTokenName: string, escrowList: EscrowConfig[]): EscrowConfig | undefined;
//# sourceMappingURL=getCurrentEscrow.d.ts.map