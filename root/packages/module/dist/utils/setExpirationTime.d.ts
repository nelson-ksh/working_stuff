/**
 * SetExpirationTime
 * @param months The number of months for Contributor to complete the
 * @returns expiration time, provided months from now as a POSIX timestamp
 */
export declare function SetExpirationTime({ months }: {
    months: number;
}): number;
//# sourceMappingURL=setExpirationTime.d.ts.map