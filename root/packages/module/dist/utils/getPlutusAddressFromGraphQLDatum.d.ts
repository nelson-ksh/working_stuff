import { GraphQLDatumValue, PlutusAddress } from 'src/types';
/**
 * From a GraphQL query, construct a PlutusAddress
 * @param graphqlUTxO
 * @returns {PlutusAddress} A Mesh-formatted Plutus Address
 */
export declare function getPlutusAddressFromGraphQLDatum(graphqlDatum: GraphQLDatumValue): PlutusAddress;
//# sourceMappingURL=getPlutusAddressFromGraphQLDatum.d.ts.map