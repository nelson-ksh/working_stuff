import { GraphQLUTxO } from 'src/types';
import { AssignmentUTxO } from '../assignment.types';
/**
 * Get the list of Assignment UTxOs in the Andamio instance.
 * @param graphqlUTxOs: GraphQLUTxO[]
 * @returns {AssignmentUTxO[]}
 */
export declare function getAssignmentUTxOs(graphqlUTxOs: GraphQLUTxO[]): AssignmentUTxO[];
//# sourceMappingURL=getAssignmentUTxOs.d.ts.map