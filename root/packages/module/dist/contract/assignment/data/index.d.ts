import { AndamioConfig } from 'src/types';
import { AssignmentContract, AssignmentInfo } from '../assignment.types';
export declare class AssignmentData implements AssignmentContract {
    private andamioConfig;
    address: string;
    contractInfo: Promise<AssignmentInfo>;
    constructor(andamioConfig: AndamioConfig);
    getAssignmentInfo(): Promise<AssignmentInfo>;
}
//# sourceMappingURL=index.d.ts.map