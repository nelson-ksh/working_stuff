import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO, Nothing } from 'src/types';
export type CommittedAssignmentId = string;
export type LearnerPkh = string;
export type txHash = string;
export type txIndex = number;
export type MaybeStudentAssignmentInfo = (Data & {
    alternative: 0;
    fields: [string];
}) | Nothing;
export type TxOutRef = Data & {
    alternative: 0;
    fields: [
        {
            alternative: 0;
            fields: [txHash];
        },
        txIndex
    ];
};
export declare type AssignmentDatum = Data & {
    alternative: 0;
    fields: [
        CommittedAssignmentId,
        MaybeStudentAssignmentInfo,
        TxOutRef,
        LearnerPkh
    ];
};
export interface AssignmentUTxO extends ContractUTxO<AssignmentDatum> {
    utxo: UTxO;
    datum: AssignmentDatum;
}
export interface AssignmentInfo extends ContractInfo<AssignmentUTxO, AssignmentDatum> {
    utxos: AssignmentUTxO[];
    data: AssignmentDatum[];
}
export interface AssignmentContract extends ContractData<AssignmentInfo> {
}
//# sourceMappingURL=assignment.types.d.ts.map