import { GraphQLUTxO } from 'src/types';
import { CourseReferenceUTxO } from '../courseReference.types';
/**
 * Get the current set of CourseReferenceUTxOs
 * @param graphqlUTxO
 * @returns {CourseReferenceUTxO[]}
 */
export declare function getCourseReferenceUTxOs(graphqlUTxOs: GraphQLUTxO[]): CourseReferenceUTxO[];
//# sourceMappingURL=getCourseReferenceUTxOs.d.ts.map