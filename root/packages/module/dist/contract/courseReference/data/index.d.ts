import { AndamioConfig } from 'src/types';
import { CourseReferenceContract, CourseReferenceInfo } from '../courseReference.types';
export declare class CourseReferenceData implements CourseReferenceContract {
    private andamioConfig;
    address: string;
    contractInfo: Promise<CourseReferenceInfo>;
    constructor(andamioConfig: AndamioConfig);
    getCourseReferenceInfo(): Promise<CourseReferenceInfo>;
}
//# sourceMappingURL=index.d.ts.map