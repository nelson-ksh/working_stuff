import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO, Nothing } from 'src/types';
import { PlutusAddress } from 'src/types';
export declare type ModuleReferenceDatum = Data & {
    alternative: 0;
    fields: [ModuleCurrencySymbol, SLT[], MaybeAssignment];
};
export type ModuleCurrencySymbol = string;
export type SLTId = string;
export type SLTContent = string;
export type SLT = Data & {
    alternative: 0;
    fields: [SLTId, SLTContent];
};
export type GraphQLSLT = {
    constructor: 0;
    fields: [{
        'bytes': string;
    }, {
        'bytes': string;
    }];
};
export type AssignmentContent = string;
export type CurrencySymbol = string;
export type PrerequisiteAssignment = string;
export type Assignment = Data & {
    alternative: 0;
    fields: [
        AssignmentContent,
        PlutusAddress[],
        CurrencySymbol[],
        PrerequisiteAssignment[]
    ];
};
export type MaybeAssignment = (Data & {
    alternative: 0;
    fields: [Assignment];
}) | Nothing;
export interface CourseReferenceUTxO extends ContractUTxO<ModuleReferenceDatum> {
    utxo: UTxO;
    datum: ModuleReferenceDatum;
}
export interface CourseReferenceInfo extends ContractInfo<CourseReferenceUTxO, ModuleReferenceDatum> {
    utxos: CourseReferenceUTxO[];
    data: ModuleReferenceDatum[];
}
export interface CourseReferenceContract extends ContractData<CourseReferenceInfo> {
}
//# sourceMappingURL=courseReference.types.d.ts.map