import { AndamioConfig, ResponseUTxOsList } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
}
export declare function CurrentContributorReferenceUTxOs({ andamioConfig, }: Props): Promise<ResponseUTxOsList>;
export {};
//# sourceMappingURL=currentContributorReferenceUTxOs.d.ts.map