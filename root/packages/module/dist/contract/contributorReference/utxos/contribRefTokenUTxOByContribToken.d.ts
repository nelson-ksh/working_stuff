import { AndamioConfig, ResponseUTxO } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    contribToken: string;
}
export declare function contribRefTokenUTxOByContribToken({ andamioConfig, contribToken, }: Props): Promise<ResponseUTxO>;
export {};
//# sourceMappingURL=contribRefTokenUTxOByContribToken.d.ts.map