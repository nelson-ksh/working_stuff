import { AndamioConfig } from 'src/types';
import { ContributorReferenceContract, ContributorReferenceInfo } from '../contributorReference.types';
export declare class ContributorReferenceData implements ContributorReferenceContract {
    private andamioConfig;
    address: string;
    contractInfo: Promise<ContributorReferenceInfo>;
    constructor(andamioConfig: AndamioConfig);
    getContributorReferenceInfo(): Promise<ContributorReferenceInfo>;
}
//# sourceMappingURL=index.d.ts.map