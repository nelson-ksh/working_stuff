import { ResponseUTxO } from 'src/types';
import { ContributorReferenceDatum } from '../contributorReference.types';
export declare function ContributorReferenceDatumByTokenName(contributorReferenceUTxO: ResponseUTxO): ContributorReferenceDatum;
//# sourceMappingURL=contributorReferenceDatumByTokenName.d.ts.map