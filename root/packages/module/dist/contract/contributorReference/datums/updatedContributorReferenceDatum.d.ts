import { GraphQLUTxO } from 'src/types';
import { ContributorReferenceDatum } from '../contributorReference.types';
export declare function UpdatedContributorReferenceDatum(contribAddress: string, contribRefTokenGraphQLUTxO: GraphQLUTxO): ContributorReferenceDatum;
//# sourceMappingURL=updatedContributorReferenceDatum.d.ts.map