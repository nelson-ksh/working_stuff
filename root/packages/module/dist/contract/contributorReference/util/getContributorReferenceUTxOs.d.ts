import { ContributorReferenceUTxO } from 'src/contract/contributorReference/contributorReference.types';
import { GraphQLUTxO } from 'src/types';
/**
 * Get the list of Contributors in the Andamio instance.
 * @param graphqlUTxO
 * @returns {string[]} The list of Contributor datum currently at the Contributor Reference Address.
 */
export declare function getContributorReferenceUTxOs(graphqlUTxOs: GraphQLUTxO[]): ContributorReferenceUTxO[];
//# sourceMappingURL=getContributorReferenceUTxOs.d.ts.map