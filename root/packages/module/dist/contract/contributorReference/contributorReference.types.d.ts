import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO } from 'src/types';
import { PlutusAddress } from 'src/types';
import { ProjectData } from '../contract.types';
export declare type ContributorReferenceDatum = Data & {
    alternative: 0;
    fields: [PlutusAddress, number];
};
export interface ContributorReferenceUTxO extends ContractUTxO<ContributorReferenceDatum> {
    utxo: UTxO;
    datum: ContributorReferenceDatum;
}
export interface ContributorReferenceInfo extends ContractInfo<ContributorReferenceUTxO, ContributorTokenHistory> {
    utxos: ContributorReferenceUTxO[];
    data: ContributorTokenHistory[];
}
export interface ContributorReferenceContract extends ContractData<ContributorReferenceInfo> {
}
export type ContributorTokenHistory = {
    contributorAssetId: string;
    txHistory: ContributorTokenTx[];
    completedProjects: ProjectData[];
};
export type ContributorTokenTx = {
    txHash: string;
    projectHash?: string;
    type: 'Commit' | 'Distribute';
    escrowAddress: string;
    includedAt: string;
    lovelace?: number;
    tokens?: number;
};
//# sourceMappingURL=contributorReference.types.d.ts.map