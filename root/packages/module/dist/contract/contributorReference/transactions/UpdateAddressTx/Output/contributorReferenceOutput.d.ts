import { ResponseUTxO, SendValue } from 'src/types';
export declare function ContributorReferenceOutput({ validatorAddress, contributorRefInput, contributorAddress, }: {
    validatorAddress: string;
    contributorRefInput: ResponseUTxO;
    contributorAddress: string;
}): SendValue;
//# sourceMappingURL=contributorReferenceOutput.d.ts.map