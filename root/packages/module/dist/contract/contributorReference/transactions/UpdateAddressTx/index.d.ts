import { BrowserWallet } from '@meshsdk/core';
import { AndamioConfig, RedeemValue, SendValue } from 'src/types';
import { ContributorReferenceDatum } from '../../contributorReference.types';
export declare class UpdateAddressTx {
    private wallet;
    private andamioConfig;
    private contribRefRefScriptUtxo;
    private contributorAddress;
    private contributorTokenName;
    private contributorTokenAsset;
    private queryContribRefTokenUtxo;
    private contributorReferenceInput;
    private contributorReferenceDatum;
    private contributorReferenceOutput;
    private contribTokenBackToContributor;
    constructor(wallet: BrowserWallet, andamioConfig: AndamioConfig);
    runQueries(): Promise<void>;
    initialize(): void;
    showContribRefDatum(): Promise<ContributorReferenceDatum | undefined>;
    showTxInfo(): Promise<{
        contractIn: RedeemValue | undefined;
        contractOut: SendValue | undefined;
        contribOut: SendValue | undefined;
    }>;
    updateAddress(): Promise<void>;
}
//# sourceMappingURL=index.d.ts.map