import { UTxO } from '@meshsdk/core';
import { RedeemValue } from 'src/types';
export declare function ContibutorReferenceInput({ inputUTxO, refUTxO, contributorAddress, }: {
    inputUTxO: UTxO;
    refUTxO: UTxO;
    contributorAddress: string;
}): RedeemValue;
//# sourceMappingURL=contributorReferenceInput.d.ts.map