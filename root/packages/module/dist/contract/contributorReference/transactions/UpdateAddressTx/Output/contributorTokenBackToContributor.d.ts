import { SendValue } from 'src/types';
export declare function ContributorTokenBackToContributor({ address, contributorAsset, }: {
    address: string;
    contributorAsset: string;
}): SendValue;
//# sourceMappingURL=contributorTokenBackToContributor.d.ts.map