import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig } from 'src/types';
export declare function commitmentDatum(selectedProject: ProjectData, andamioConfig: AndamioConfig): {
    alternative: number;
    fields: (string | number | {
        alternative: number;
        fields: {
            alternative: number;
            fields: string[];
        }[];
    })[];
};
//# sourceMappingURL=commitmentDatum.d.ts.map