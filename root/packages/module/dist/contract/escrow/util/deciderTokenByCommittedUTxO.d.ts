import { BrowserWallet, UTxO } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    connectedWallet: BrowserWallet;
    committedUTxO: UTxO;
}
export declare function DeciderTokenByCommittedUTxO({ andamioConfig, connectedWallet, committedUTxO, }: Props): Promise<string>;
export {};
//# sourceMappingURL=deciderTokenByCommittedUTxO.d.ts.map