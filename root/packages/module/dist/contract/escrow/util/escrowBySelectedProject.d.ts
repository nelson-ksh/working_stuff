import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, EscrowConfig } from 'src/types';
interface Props {
    selectedProject: ProjectData;
    andamioConfig: AndamioConfig;
}
export declare function escrowBySelectedProject({ selectedProject, andamioConfig, }: Props): EscrowConfig;
export {};
//# sourceMappingURL=escrowBySelectedProject.d.ts.map