import { UTxO } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    committedUTxO: UTxO;
}
interface ReturnProps {
    lovelaceAmount: number;
    projectAssetAmmount: number;
}
export declare function RewardsByCommittedUTxO({ andamioConfig, committedUTxO, }: Props): ReturnProps;
export {};
//# sourceMappingURL=rewardsByCommittedUTxO.d.ts.map