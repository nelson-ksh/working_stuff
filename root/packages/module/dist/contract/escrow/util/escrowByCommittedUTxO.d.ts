import { UTxO } from '@meshsdk/core';
import { AndamioConfig, EscrowConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    committedUTxO: UTxO;
}
export declare function EscrowByCommittedUTxO({ andamioConfig, committedUTxO, }: Props): EscrowConfig;
export {};
//# sourceMappingURL=escrowByCommittedUTxO.d.ts.map