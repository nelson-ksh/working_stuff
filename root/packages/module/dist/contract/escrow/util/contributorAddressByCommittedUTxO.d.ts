import { UTxO } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    committedUTxO: UTxO;
}
export declare function ContributorAddressByCommittedUTxO({ andamioConfig, committedUTxO, }: Props): Promise<string>;
export {};
//# sourceMappingURL=contributorAddressByCommittedUTxO.d.ts.map