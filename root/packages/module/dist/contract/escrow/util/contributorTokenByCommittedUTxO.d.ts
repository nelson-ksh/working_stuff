import { UTxO } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    committedUTxO: UTxO;
}
export declare function ContributorTokenByCommittedUTxO({ andamioConfig, committedUTxO, }: Props): string;
export {};
//# sourceMappingURL=contributorTokenByCommittedUTxO.d.ts.map