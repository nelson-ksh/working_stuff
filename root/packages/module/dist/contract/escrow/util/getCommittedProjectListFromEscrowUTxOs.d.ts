import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, GraphQLUTxO } from 'src/types';
export declare function getCommittedProjectListFromEscrowUTxOs(andamioConfig: AndamioConfig, graphqlUTxOs: GraphQLUTxO[], contributorPolicyId: string): ProjectData[];
//# sourceMappingURL=getCommittedProjectListFromEscrowUTxOs.d.ts.map