import { AndamioConfig, ResponseUTxO } from 'src/types';
export declare function escrowByContractTokenUTxO(andamioConfig: AndamioConfig, treasuryContractTokenUtxo: ResponseUTxO): import("src/types").EscrowConfig;
//# sourceMappingURL=escrowByContractTokenUTxO.d.ts.map