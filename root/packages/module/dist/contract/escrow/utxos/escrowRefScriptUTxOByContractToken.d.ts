import { AndamioConfig } from 'src/types';
export declare function escrowRefScriptUTxOByContractToken(andamioConfig: AndamioConfig, contractToken: string): {
    input: {
        outputIndex: number;
        txHash: string;
    };
    output: {
        address: string;
        amount: {
            unit: string;
            quantity: string;
        }[];
        scriptRef: string;
    };
};
//# sourceMappingURL=escrowRefScriptUTxOByContractToken.d.ts.map