import { UTxO } from '@meshsdk/core';
import { EscrowConfig } from 'src/types';
export declare function escrowRefScriptUTxOByEscrow(escrow: EscrowConfig): UTxO;
//# sourceMappingURL=escrowRefScriptUTxOByEscrow.d.ts.map