import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, ResponseUTxO } from 'src/types';
export declare function queryCommitmentUTxOBySelectedProject(andamioConfig: AndamioConfig, selectedProject: ProjectData): Promise<ResponseUTxO>;
//# sourceMappingURL=queryCommitmentUTxOBySelectedProject.d.ts.map