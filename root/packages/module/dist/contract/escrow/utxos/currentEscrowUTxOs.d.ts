import { UTxO } from '@meshsdk/core';
import { AndamioConfig, EscrowConfig, GraphQLUTxO } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    escrow: EscrowConfig;
}
interface ReturnProps {
    graphQLUTxOs: GraphQLUTxO[];
    meshUTxOs: UTxO[];
}
export declare function CurrentEscrowUTxOs({ andamioConfig, escrow, }: Props): Promise<ReturnProps>;
export {};
//# sourceMappingURL=currentEscrowUTxOs.d.ts.map