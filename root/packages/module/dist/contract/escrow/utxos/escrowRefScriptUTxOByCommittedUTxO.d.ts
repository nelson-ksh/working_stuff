import { UTxO } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
    committedUTxO: UTxO;
}
export declare function escrowRefScriptUTxOByCommittedUTxO({ andamioConfig, committedUTxO, }: Props): UTxO;
export {};
//# sourceMappingURL=escrowRefScriptUTxOByCommittedUTxO.d.ts.map