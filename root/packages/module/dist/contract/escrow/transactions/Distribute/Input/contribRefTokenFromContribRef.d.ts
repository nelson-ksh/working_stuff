import { UTxO } from '@meshsdk/core';
import { RedeemValue, ResponseUTxO } from 'src/types';
export declare function contribRefTokenFromContribRef(contribRefTokenUTxO: ResponseUTxO, contribRefScriptUTxO: UTxO): RedeemValue;
//# sourceMappingURL=contribRefTokenFromContribRef.d.ts.map