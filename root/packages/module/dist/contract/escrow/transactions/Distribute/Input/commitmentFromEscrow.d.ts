import { UTxO } from '@meshsdk/core';
import { RedeemValue, ResponseUTxO } from 'src/types';
export declare function commitmentFromEscrow(commitmentUTxO: ResponseUTxO, escrowRefScriptUTxO: UTxO): RedeemValue;
//# sourceMappingURL=commitmentFromEscrow.d.ts.map