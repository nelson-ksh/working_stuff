import { ProjectData } from 'src/contract/contract.types';
import { SendValue } from 'src/types';
export declare function contribTokenToContrib(selectedProject: ProjectData, contribAddress: string): SendValue;
//# sourceMappingURL=contribTokenToContrib.d.ts.map