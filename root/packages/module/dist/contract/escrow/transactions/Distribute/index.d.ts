import { BrowserWallet } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, ContractTransaction, ReferenceScriptUTxOs, TxInputs, TxOutputs } from 'src/types';
export declare class DistributeTx implements ContractTransaction {
    private wallet;
    private selectedProject;
    private andamioConfig;
    private initialized;
    referenceScriptUTxOs: ReferenceScriptUTxOs;
    inputs: TxInputs;
    outputs: TxOutputs;
    private ESCROW;
    private changeAddress;
    private connectedDeciderToken;
    private commitmentUTxO;
    private contribRefTokenUTxO;
    private contribAddress;
    private treasuryContractTokenUTxO;
    constructor(wallet: BrowserWallet, selectedProject: ProjectData, andamioConfig: AndamioConfig);
    runQueries(): Promise<void>;
    initialize(): void;
    runTx(): Promise<string>;
    txDetails(): {
        commitmentFromEscrow: {
            redeemValue: import("src/types").RedeemValue;
        };
        contribRefTokenFromContribRef: {
            redeemValue: import("src/types").RedeemValue;
        };
        updatedContribRefTokenToContribRef: {
            sendValue: import("src/types").SendValue;
        };
        rewardToContrib: {
            sendValue: import("src/types").SendValue;
        };
        contribTokenToContrib: {
            sendValue: import("src/types").SendValue;
        };
        deciderToConnectedWallet: {
            sendValue: import("src/types").SendValue;
        };
        treasuryContractTokenUTxO: {
            setTxRefInputs: import("@meshsdk/core").UTxO[];
        };
    };
}
//# sourceMappingURL=index.d.ts.map