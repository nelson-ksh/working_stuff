import { UTxO } from '@meshsdk/core';
import { AndamioConfig, SendValue } from 'src/types';
export declare function rewardToContrib(andamioConfig: AndamioConfig, contributorAddress: string, commitmentUTxO: UTxO): SendValue;
//# sourceMappingURL=rewardToContrib.d.ts.map