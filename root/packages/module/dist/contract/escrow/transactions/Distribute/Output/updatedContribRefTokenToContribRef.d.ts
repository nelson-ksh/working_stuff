import { AndamioConfig, ResponseUTxO, SendValue } from 'src/types';
export declare function updatedContribRefTokenToContribRef(andamioConfig: AndamioConfig, contribRefTokenUTxO: ResponseUTxO, contribAddress: string): SendValue;
//# sourceMappingURL=updatedContribRefTokenToContribRef.d.ts.map