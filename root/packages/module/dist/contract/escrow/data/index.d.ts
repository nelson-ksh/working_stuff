import { AndamioConfig } from 'src/types';
import { EscrowContractData, EscrowInfo } from '../escrow.types';
export declare class EscrowData implements EscrowContractData {
    private andamioConfig;
    private escrowIndex;
    address: string;
    contractInfo: Promise<EscrowInfo>;
    constructor(andamioConfig: AndamioConfig, escrowIndex: number);
    /**
     * Replaces graphqlCommitmentsQuery, and returns the new EscrowInfo Type
     * Todo: refactor AndamioContext in starter to consume this data
     * @returns Andamio-ready Commitments[] list
     */
    getEscrowInfo(addressIndex: number): Promise<EscrowInfo>;
}
//# sourceMappingURL=index.d.ts.map