import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO } from 'src/types';
import { PlutusAddress } from 'src/types';
import { ProjectData } from '../contract.types';
export declare type CommitmentDatum = {
    contributorPkh: string;
    lovelace: number;
    gimbals: number;
    expirationTime: number;
    projectHash: string;
};
export type ProjectDetailsDatum = Data & {
    alternative: 0;
    fields: [string, number, Owner, OwnerInfo];
};
export type Owner = Data & {
    alternative: 0;
    fields: [
        {
            alternative: 1;
            fields: [string];
        },
        {
            alternative: 1;
            fields: [];
        }
    ];
};
export type OwnerInfo = Data & {
    alternative: 0;
    fields: [];
};
export interface EscrowUTxO extends ContractUTxO<ProjectDetailsDatum> {
}
export interface EscrowInfo extends ContractInfo<EscrowUTxO, ProjectData> {
    utxos: EscrowUTxO[];
    data: ProjectData[];
}
export interface EscrowContractData extends ContractData<EscrowInfo> {
}
export type ProjectCommitmentDetails = {
    projectHash: string;
    projectTime: number;
    owner: PlutusAddress;
    ownerInfo: Data;
    contributorTokenName: string;
    utxo: UTxO;
};
//# sourceMappingURL=escrow.types.d.ts.map