import { AndamioConfig } from 'src/types';
import { LearnerReferenceContract, LearnerReferenceInfo } from '../learnerReference.types';
export declare class LearnerReferenceData implements LearnerReferenceContract {
    private andamioConfig;
    address: string;
    contractInfo: Promise<LearnerReferenceInfo>;
    constructor(andamioConfig: AndamioConfig);
    getLearnerReferenceInfo(): Promise<LearnerReferenceInfo>;
}
//# sourceMappingURL=index.d.ts.map