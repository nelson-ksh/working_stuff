import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO } from 'src/types';
import { PlutusAddress } from 'src/types';
/**
 * Add documentation to show how this datum is used. Repeat for each datum.
 */
export declare type LearnerReferenceDatum = Data & {
    alternative: 0;
    fields: [
        PlutusAddress,
        CompletedAssignment[],
        LearnerInfo,
        LearnerCurrencySymbol,
        PointToCurrencySymbol[]
    ];
};
export type CompletedAssignment = string;
export type LearnerInfo = string;
export type LearnerCurrencySymbol = string;
export type PointToCurrencySymbol = string;
export interface LearnerReferenceUTxO extends ContractUTxO<LearnerReferenceDatum> {
    utxo: UTxO;
    datum: LearnerReferenceDatum;
}
export interface LearnerReferenceInfo extends ContractInfo<LearnerReferenceUTxO, LearnerReferenceDatum> {
    utxos: LearnerReferenceUTxO[];
    data: LearnerReferenceDatum[];
}
export interface LearnerReferenceContract extends ContractData<LearnerReferenceInfo> {
}
//# sourceMappingURL=learnerReference.types.d.ts.map