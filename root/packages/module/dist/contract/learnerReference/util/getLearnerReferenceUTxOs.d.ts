import { GraphQLUTxO } from 'src/types';
import { LearnerReferenceUTxO } from '../learnerReference.types';
/**
 * Get the list of Learners in the Andamio instance.
 * @param graphqlUTxO
 * @returns {LearnerReferenceUTxO[]}
 */
export declare function getLearnerReferenceUTxOs(graphqlUTxOs: GraphQLUTxO[]): LearnerReferenceUTxO[];
//# sourceMappingURL=getLearnerReferenceUTxOs.d.ts.map