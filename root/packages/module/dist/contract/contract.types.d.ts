export declare type ProjectData = {
    contractToken: string;
    projectHashId: string;
    expirationTime: number;
    adaAmount: number;
    projectTokenAmount: number;
    contributorAsset?: string;
};
export declare type CommitmentTxMetadata = {
    id: string;
    hash: string;
    expTime: number;
    txType: string;
    contributor?: string;
};
//# sourceMappingURL=contract.types.d.ts.map