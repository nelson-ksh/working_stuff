import { UTxO } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
import { RedeemValue } from 'src/types';
export declare function contractTokenFromTreasury(selectedProject: ProjectData, treasuryContractTokenUTxO: UTxO, treasuryRefScriptUTxO: UTxO): RedeemValue;
//# sourceMappingURL=contractTokenFromTreasury.d.ts.map