import { UTxO } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
import { RedeemValue } from 'src/types';
export declare function fundsFromTreasury(selectedProject: ProjectData, treasuryFundUTxO: UTxO, treasuryRefScriptUTxO: UTxO): RedeemValue;
//# sourceMappingURL=fundsFromTreasury.d.ts.map