import { AssetExtended } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, EscrowConfig, SendValue } from 'src/types';
export declare function commitmentToEscrow(selectedProject: ProjectData, andamioConfig: AndamioConfig, escrow: EscrowConfig, connectedContribToken: AssetExtended): SendValue;
//# sourceMappingURL=commitmentToEscrow.d.ts.map