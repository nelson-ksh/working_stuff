import { BrowserWallet } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
import { ContractTransaction, TxInputs, TxOutputs, ReferenceScriptUTxOs } from 'src/types';
import { ProjectData } from 'src/contract/contract.types';
export declare class CommitTx implements ContractTransaction {
    private wallet;
    private selectedProject;
    private andamioConfig;
    private initialized;
    referenceScriptUTxOs: ReferenceScriptUTxOs;
    inputs: TxInputs;
    outputs: TxOutputs;
    private connectedContribToken;
    private contribRefTokenUTxO;
    private treasuryContractTokenUTxO;
    private treasuryFundUTxOs;
    private escrow;
    constructor(wallet: BrowserWallet, selectedProject: ProjectData, andamioConfig: AndamioConfig);
    runQueries(): Promise<void>;
    initialize(): void;
    runTx(): Promise<string>;
    txDetails(): {
        fundsFromTreasury: {
            redeemValue: import("src/types").RedeemValue;
        };
        contractTokenFromTreasury: {
            redeemValue: import("src/types").RedeemValue;
        };
        residueFundsToTreasury: {
            sendValue: import("src/types").SendValue;
        };
        contractTokenBackToTreasury: {
            sendValue: import("src/types").SendValue;
        };
        commitmentToEscrow: {
            sendValue: import("src/types").SendValue;
        };
        connectedRefTokenUTxO: {
            setTxRefInputs: import("@meshsdk/core").UTxO[];
        };
    };
}
//# sourceMappingURL=index.d.ts.map