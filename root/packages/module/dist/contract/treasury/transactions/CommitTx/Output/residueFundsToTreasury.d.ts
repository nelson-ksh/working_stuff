import { UTxO } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, SendValue } from 'src/types';
export declare function residueFundsToTreasury(selectedProject: ProjectData, andamioConfig: AndamioConfig, treasuryFundUTxO: UTxO): SendValue;
//# sourceMappingURL=residueFundsToTreasury.d.ts.map