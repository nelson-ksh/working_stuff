import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, ResponseUTxO, SendValue } from 'src/types';
export declare function contractTokenBackToTreasury(selectedProject: ProjectData, andamioConfig: AndamioConfig, treasuryContractTokenUTxO: ResponseUTxO): SendValue;
//# sourceMappingURL=contractTokenBackToTreasury.d.ts.map