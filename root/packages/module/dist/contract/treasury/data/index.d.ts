import { AndamioConfig } from 'src/types';
import { TreasuryContractData, TreasuryInfo } from '../treasury.types';
export declare class TreasuryData implements TreasuryContractData {
    private andamioConfig;
    address: string;
    contractInfo: Promise<TreasuryInfo>;
    constructor(andamioConfig: AndamioConfig);
    /**
     *
     * @returns Andamio-ready Treasury data
     */
    getTreasuryInfo(): Promise<TreasuryInfo>;
}
//# sourceMappingURL=index.d.ts.map