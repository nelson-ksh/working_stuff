import { AndamioConfig, ResponseUTxO } from 'src/types';
export declare function queryContractTokenUTxOByContractToken(andamioConfig: AndamioConfig, contractToken: string): Promise<ResponseUTxO>;
//# sourceMappingURL=queryContractTokenUTxOByContractToken.d.ts.map