import { EscrowConfig, ResponseUTxO, ResponseUTxOsList } from 'src/types';
interface Props {
    escrow: EscrowConfig;
    currentTreasuryUTxOs: ResponseUTxOsList;
}
export declare function treasuryContractTokenUTxOByEscrow({ escrow, currentTreasuryUTxOs, }: Props): ResponseUTxO;
export {};
//# sourceMappingURL=treasuryContractTokenUTxOByEscrow.d.ts.map