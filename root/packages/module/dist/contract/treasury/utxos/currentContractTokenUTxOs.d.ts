import { ResponseUTxOsList } from 'src/types';
interface Props {
    currentTreasuryUTxOs: ResponseUTxOsList;
}
export declare function currentContractTokenUTxOs({ currentTreasuryUTxOs, }: Props): ResponseUTxOsList;
export {};
//# sourceMappingURL=currentContractTokenUTxOs.d.ts.map