import { ProjectData } from 'src/contract/contract.types';
import { AndamioConfig, ResponseUTxO, ResponseUTxOsList } from 'src/types';
export declare function treasuryContractTokenUTxOBySelectedProject(selectedProject: ProjectData, andamioConfig: AndamioConfig, currentTreasuryUTxOs: ResponseUTxOsList): ResponseUTxO;
//# sourceMappingURL=treasuryContractTokenUTxOBySelectedProject.d.ts.map