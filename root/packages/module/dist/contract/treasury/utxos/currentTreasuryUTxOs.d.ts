import { AndamioConfig, ResponseUTxOsList } from 'src/types';
interface Props {
    andamioConfig: AndamioConfig;
}
export declare function currentTreasuryUTxOs({ andamioConfig, }: Props): Promise<ResponseUTxOsList>;
export {};
//# sourceMappingURL=currentTreasuryUTxOs.d.ts.map