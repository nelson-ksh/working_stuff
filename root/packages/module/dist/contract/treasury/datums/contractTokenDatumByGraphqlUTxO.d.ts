import { GraphQLUTxO } from 'src/types';
import { ContractTokenDatum } from '../treasury.types';
/**
 * Returns a formatted ContractTokenDatum to accompany the Contract UTxO
 * @param graphqlUTxO
 * @returns {ContractTokenDatum}
 */
export declare function contractTokenDatumByGraphqlUTxO(graphqlUTxO: GraphQLUTxO): ContractTokenDatum;
//# sourceMappingURL=contractTokenDatumByGraphqlUTxO.d.ts.map