import { ProjectData } from 'src/contract/contract.types';
import { GraphQLUTxO } from 'src/types';
interface Props {
    selectedProject: ProjectData;
    contractTokenUtxo: GraphQLUTxO;
}
export declare function updatedContractTokenDatum({ selectedProject, contractTokenUtxo, }: Props): {
    alternative: number;
    fields: import("@meshsdk/core").Data[];
} & {
    alternative: 1;
    fields: [{
        alternative: 0;
        fields: [import("../treasury.types").TreasuryProject[], string[]];
    }];
};
export {};
//# sourceMappingURL=updatedContractTokenDatum.d.ts.map