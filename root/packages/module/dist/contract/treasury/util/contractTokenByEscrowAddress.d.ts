import { AndamioConfig } from 'src/types';
/**
 * @returns assetId of the Contract Token
 */
export declare function contractTokenByEscrowAddress(andamioConfig: AndamioConfig, escrowAddress: string): string;
//# sourceMappingURL=contractTokenByEscrowAddress.d.ts.map