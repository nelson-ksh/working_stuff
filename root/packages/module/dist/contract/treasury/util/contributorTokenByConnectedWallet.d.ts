import { BrowserWallet } from '@meshsdk/core';
import { AndamioConfig } from 'src/types';
interface Props {
    wallet: BrowserWallet;
    andamioConfig: AndamioConfig;
}
interface ReturnProps {
    unit: string;
    assetName: string;
}
export declare function contributorTokenByConnectedWallet({ wallet, andamioConfig, }: Props): Promise<ReturnProps>;
export {};
//# sourceMappingURL=contributorTokenByConnectedWallet.d.ts.map