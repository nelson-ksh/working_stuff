import { ResponseUTxOsList } from 'src/types';
interface Props {
    currentContractTokenUtxos: ResponseUTxOsList;
}
interface Project {
    fields: [
        {
            bytes: string;
        },
        {
            int: number;
        },
        {
            int: number;
        },
        {
            int: number;
        }
    ];
    constructor: number;
}
interface ReturnProps {
    contractTokenName: string;
    projects: Project[];
}
export declare function currentProjectsInContractTokenUtxos({ currentContractTokenUtxos, }: Props): ReturnProps[];
export {};
//# sourceMappingURL=currentProjectsInContractTokenUtxos.d.ts.map