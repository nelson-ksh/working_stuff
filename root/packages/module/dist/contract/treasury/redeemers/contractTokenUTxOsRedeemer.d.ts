import { Action } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
interface Props {
    selectedProject: ProjectData;
}
export declare function contractTokenUTxOsRedeemer({ selectedProject, }: Props): Partial<Action>;
export {};
//# sourceMappingURL=contractTokenUTxOsRedeemer.d.ts.map