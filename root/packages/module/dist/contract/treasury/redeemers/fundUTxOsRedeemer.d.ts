import { Action } from '@meshsdk/core';
import { ProjectData } from 'src/contract/contract.types';
interface Props {
    selectedProject: ProjectData;
}
export declare function fundUTxOsRedeemer({ selectedProject }: Props): Partial<Action>;
export {};
//# sourceMappingURL=fundUTxOsRedeemer.d.ts.map