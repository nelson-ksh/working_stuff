import { Data, UTxO } from '@meshsdk/core';
import { ContractData, ContractInfo, ContractUTxO } from 'src/types';
import { ProjectData } from '../contract.types';
export type FundDatum = Data & {
    alternative: 0;
    fields: [];
};
export type ContractTokenDatum = Data & {
    alternative: 1;
    fields: [
        {
            alternative: 0;
            fields: [TreasuryProject[], string[]];
        }
    ];
};
export declare type TreasuryProject = Data & {
    alternative: 0;
    fields: [string, number, number, number];
};
export interface TreasuryUTxO<T> extends ContractUTxO<T> {
    utxo: UTxO;
    datum: T;
    contractToken?: string;
}
export interface TreasuryInfo extends ContractInfo<TreasuryUTxO<FundDatum | ContractTokenDatum>, ProjectData> {
    utxos: TreasuryUTxO<FundDatum | ContractTokenDatum>[];
    data: ProjectData[];
    numFundUTxOs: number;
    numContractUTxOs: number;
    fundUTxOs: TreasuryUTxO<FundDatum>[];
    contractUTxOs: TreasuryUTxO<ContractTokenDatum>[];
    totalLovelace: number;
    totalTokens: number;
}
export interface TreasuryContractData extends ContractData<TreasuryInfo> {
}
//# sourceMappingURL=treasury.types.d.ts.map